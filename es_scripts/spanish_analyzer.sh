#!/bin/bash
#en keyword_marker poner una lista de palabras en español que no queremos que se modifiquen por el stemmer
#lowercase pasa todo a minusculas
#asciifolding quita acentos... pero no funciona :( --- al parecer hace interferencia ponerlo junto con spanish_stemmer, ya que este se fija que las terminaciones tengan tilde para considerarla algo que eliminar
curl -XPUT 'localhost:9200/users?pretty' -H 'Content-Type: application/json' -d'
{
  "settings": {
    "analysis": {
      "filter": {
        "spanish_stop": {
          "type":       "stop",
          "stopwords":  "_spanish_"
        },
        "spanish_keywords": {
          "type":       "keyword_marker",
          "keywords":   [""]
        },
        "spanish_stemmer": {
          "type":       "stemmer",
          "language":   "light_spanish"
        }
      },
      "analyzer": {
        "spanish": {
          "tokenizer":  "standard",
          "filter": [
            "lowercase",
            "spanish_stop",
            "spanish_keywords",
            "spanish_stemmer"
          ]
        }
      }
    }
  },
  "mappings": {
    "user": {
      "_all": {
        "analyzer": "spanish",
        "search_analyzer": "spanish"
      }
    }
  }
}
'
