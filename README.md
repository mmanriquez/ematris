# **incubando.me** #

## **Requisitos** ##

1. MongoDB (2.6.10): sitio oficial [https://www.mongodb.com/es](https://www.mongodb.com/es)
2. Node.js (6.9.2): sitio oficial [https://nodejs.org/es/](https://nodejs.org/es/)
3. ElasticSearch(5.4.0): sitio oficial [https://www.elastic.co/downloads/elasticsearch](https://www.elastic.co/downloads/elasticsearch)

## **Instalación** ##

Una vez instalados los requisitos, se debe clonar el repositorio en tu servidor con el comando :

```
#!console

git clone https://bitbucket.org/mmanriquez/ematris
```

Entra al directorio creado:


```
#!console

cd ematris
```

Luego es necesario instalar todas las dependencias:


```
#!console

npm install
```

Hay dos dependencias que no es instalan automáticamente, por lo que tendremos que instalarlas nosotros mismos:

```
#!console

sudo apt-get install imagemagick
npm install image-size --global
```

Haz correr [mongoDB](https://docs.mongodb.com/manual/tutorial/manage-mongodb-processes/) y [elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/5.4/index.html), luego configura elasticsearch:

```
#!console

.es_scripts/spanish_analyzer.sh
```



Finalmente deberemos definir la variable de entorno KEY_EMATRIS (en [windows](http://www.dowdandassociates.com/blog/content/howto-set-an-environment-variable-in-windows-command-line-and-registry/) se realiza distinto que en [linux](https://www.cyberciti.biz/faq/set-environment-variable-linux/)), que es la llave que se utilizará para encriptar y desencriptar la información de los usuarios:


Luego para iniciar el servicio ingresa:


```
#!console

npm start
```

el sitio estará corriendo por defecto en el puerto 3000, localmente el sitio se verá en


```
#!console

http://localhost:3000
```


Para quitar el uso del puerto 3000, y utilizar por defecto el puerto 80, se puede ejecutar la aplicación de la forma:
```
#!console

sudo PORT=80 npm start
```

pero esto no es recomendable para un ambiente en producción, lo mejor es redireccionar las llamadas utilizando una herramienta cortafuegos llamada iptables:

```
#!console

sudo iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000
```
