(function() {

  angular
    .module('meanApp')
    .controller('profileCtrl', profileCtrl);

  //á
  profileCtrl.$inject = ['$location', 'authentication','$http','$routeParams'];
  function profileCtrl($location, authentication ,$http,$routeParams) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    vm.user = authentication.currentUser();
    //console.log(vm.user);
    vm.edit = false;
    var sociales = function(user){
      vm.user.redes = [];
      user.redesSociales.forEach(function(red, index){
        var base = "images/redes/"
        var imagenes = {blogger: base+"blogger.svg"
        ,flickr:base+"flickr.svg"
        ,kickstarter:base+"kickstarter.svg"
        //,skype:base+"skype.svg"
        ,facebook:base+"square-facebook.svg"
        ,plus:base+"square-google-plus.svg"
        ,linkedin:base+"square-linkedin.svg"
        ,twitter:base+"square-twitter.svg"
        ,vimeo:base+"vimeo.svg"
        ,youtube:base+"youtube.svg"
        };
        //vm.user.redesSociales[index].url;
        var social = {};
        social.url = red;
        console.log("Chequeando "+red);
        for (var variable in imagenes) {
          if (red.includes(variable)) {
            console.log("Es de "+variable);
            //vm.user.redesSociales[index].img = imagenes[variable];
            social.img = imagenes[variable];
          }
        }
        if(!social.img){
          social.img = base+"network.svg";
        }
        vm.user.redes.push(social);
      })
    };

    vm.noExiste = false;
    if(!$routeParams.userId || ($routeParams.userId && $routeParams.userId == vm.user._id)){
      $http.get('api/public/profile',{params:{userId: vm.user._id}})
      .success(function(response){
        console.log(response);
        vm.user = response;
        vm.titulo = "Perfil de "+vm.user.name;
        sociales(vm.user);
        console.log(vm.user.redes);
        cargarFoto(vm.user._id);
      })
      .error(function(errData){
        console.log(errData);
        console.log("Error:"+errData.error);
        console.log(errData.err);
      });
      vm.edit = true;
    }
    else if($routeParams.userId && $routeParams.userId != vm.user._id){
      $http.get('api/public/profile',{params:{userId: $routeParams.userId}})
      .success(function(response){
        console.log(response);
        vm.user = response;
        vm.titulo = "Perfil de "+vm.user.name;
        sociales(vm.user);
        console.log(vm.user.redes);
        cargarFoto(vm.user._id);
      })
      .error(function(errData, status){
        console.log("Error:"+errData.error);
        if(status==404){
          vm.noExiste = true;
          vm.titulo = errData.error;
        }
      });
    }

    vm.subirFoto = function(){
      var file = vm.foto;
      console.log('file is:');
      console.dir(file);

      var fd = new FormData();//necesario para hacer un post
      fd.append('file', file);
      fd.append('userId',vm.user._id);

      $http.post("api/foto", fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
      .success(function(){
        console.log("Se subio bien la foto");
        cargarFoto(vm.user._id);
        vm.mostrar = false;
      })
      .error(function(errData){
        console.log("Error:"+errData.error);
        console.log(errData.err);
      });
    }
    var cargarFoto = function(userId){
      var now = new Date().getTime();
      $http.get("/api/userImage", {params:{userId:userId}})
      .success(function(response){
        vm.user.image = response.ruta+"?ts="+now;
      }).error(function(errData){
        vm.user.image = "/profiles/male.png";
      });
    };
  }
})();
