(function() {

  angular
    .module('meanApp')
    .controller('listDocCtrl', listDocCtrl);

  listDocCtrl.$inject = ['$http','$routeParams', 'projectService','$location','authentication'];
  function listDocCtrl($http,$routeParams, projectService,$location,authentication) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
    vm.proy = {};

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.hayTareas = false;
      //console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
    });


    vm.documentos = [];
    vm.columnas = [];
    //$http.get("api/documentos",{params:{proyId:$routeParams['proyId']}})
    $http.get("api/tareas/documentos",{params:{proyId:$routeParams['proyId']}})
    .then(function(response){
      //console.log(response.data);
      vm.columnas = response.data;
      for (var k = 0; k < vm.columnas.length; k++) {
        for (var i = 0; i < vm.columnas[k].tareas.length; i++) {
          for (var j = 0; j < vm.columnas[k].tareas[i].documentos.length; j++) {
            vm.columnas[k].tareas[i].documentos[j].url = "documents/"+vm.columnas[k].tareas[i].documentos[j]._id+"."+vm.columnas[k].tareas[i].documentos[j].nombre.split(".")[1];
            vm.columnas[k].tareas[i].documentos[j].creador = vm.columnas[k].tareas[i].creador;
            vm.columnas[k].tareas[i].documentos[j].tareaUrl = "/task/"+vm.proy._id+"/"+vm.columnas[k].tareas[i]._id;
            vm.columnas[k].tareas[i].documentos[j].tareaName = vm.columnas[k].tareas[i].nombre;
            vm.documentos.push(vm.columnas[k].tareas[i].documentos[j]);
          }
        }
      }
      //console.log(vm.documentos);
    }
    ,function(errData){
      console.log("ERROR:"+errData.error);
      console.log(errData.err);
    });
  };
})();
