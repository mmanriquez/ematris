(function() {

  angular
    .module('meanApp')
    .controller('projectNew3Ctrl', projectNew3Ctrl);

  projectNew3Ctrl.$inject = ['$http','$interval','$location', 'authentication','$routeParams','$window'];
  function projectNew3Ctrl($http,$interval,$location, authentication,$routeParams,$window) {
    var vm = this;

    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var user = authentication.currentUser();
    vm.datos = JSON.parse($window.localStorage['mm-nuevoProyecto']);
    vm.mostrarInvitacion = false;





    vm.volver = function(){
      window.history.back();
    };

  

    vm.consulta = "";
    vm.resultados = [];
    vm.participantes = [];
    vm.invitados = [];
    vm.invitacion = "";
    vm.consultarParticipantes = function(){
      if(vm.consulta!=""){
        $http.get("/api/proyecto/queryParticipantes",{params:{consulta:vm.consulta}})
        .success(function(response){
          console.log(response);
          vm.resultados = response;
          /*if(vm.resultados.length==1 && vm.consulta==vm.resultados[0]._source.name){
            vm.participantes.push(vm.resultados[0]);
            vm.consulta="";
            vm.resultados=[];
          }*/
          for (var i = 0; i < vm.resultados.length; i++) {
            if(vm.consulta==vm.resultados[i]._source.name){
              vm.participantes.push(vm.resultados[0]);
              vm.consulta="";
              vm.resultados=[];
              break;
            }
          }
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    };

    vm.invitacion = "";
    vm.nameInvitado = "";
    vm.invitar = function(){
      if(vm.invitacion!=""){
        /*$http.post("/api/invitar",{correo:vm.invitacion})
        .success(function(response){
          console.log(response);
          vm.invitados.push(response);
          vm.invitacion = "";
          vm.textoInvitacion = "";
          vm.mostrarInvitacion = false;
        })
        .error(function(errData){
          console.log("ERROR:");
          console.log(errData);
        });*/
        vm.invitados.push({correo:vm.invitacion,name:vm.nameInvitado, invitacion:vm.textoInvitacion});
        vm.invitacion = "";
        vm.textoInvitacion = "";
        vm.nameInvitado = "";
        vm.mostrarInvitacion = false;
      }
    };

    vm.quitarInvitado = function(invitado){
      var pos = vm.invitados.indexOf(invitado);
      if(pos >= 0){
        vm.invitados.splice(pos,1);
      }
    };

    vm.quitar = function(participante){
      //vm.participantes.remove(participante);
      vm.participantes.splice(vm.participantes.indexOf(participante),1);
    }

    vm.saved = false;
    vm.onSubmit = function(){

      if(vm.participantes.length==0){
        vm.mensaje = "Debe elegir algun participante";
        return;
      }
      vm.datos.participantes = [];
      for (var i = 0; i < vm.participantes.length; i++) {
        vm.datos.participantes.push(vm.participantes[i]._id);
      }
      /*var mentoresIds = [];
      for (var i = 0; i < vm.datos.mentores.length; i++) {
        mentoresIds.push(vm.participantes[i]._id);
      }
      vm.datos.mentoresIds = mentoresIds;*/
      //vm.datos.participantes = vm.participantes;
      vm.datos.invitados = vm.invitados;
      console.log(vm.datos);
      $http.post("/api/proyecto",vm.datos)
      .then(function success(response){
        $window.localStorage.removeItem('mm-nuevoProyecto');
        vm.saved = true;
        $interval(function () {
          vm.saved = false;
          //console.log(response);
          $location.path("muro/"+response.data._id);
        }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
      },function error(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });
    }

    var cargarFoto = function(usuario){
      $http.get("/api/userImage", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.imagen = response.ruta;
      });
    };

  };

})();
