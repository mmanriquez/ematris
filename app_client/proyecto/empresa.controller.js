//controlador de la vista de un proyecto desde el punto de vista de un FACILITADOR

(function() {

  angular
    .module('meanApp')
    .controller('empresaCtrl', empresaCtrl);

  empresaCtrl.$inject = ['$http','$routeParams','$location','$interval','$sce','authentication', 'projectService'];
  function empresaCtrl($http,$routeParams,$location,$interval,$sce,authentication,projectService) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }

    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
    vm.proy = {};

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.hayTareas = false;
      //console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
    });

    vm.user = authentication.currentUser();
    //console.log(vm.user.rol=='emprendedor');

    vm.datos = {_id:$routeParams['empId'],urlvideo:"",quehace:"",industria:"",estado:"",oportunidad:"",competencia:"",solucion:"",clientes:"",modelo:"",impacto:""};


    var cargarFoto = function(usuario){
      $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.thumbnail = response.ruta;
      });
    };

    $http.get("/api/empresa",{params:{empId:$routeParams['empId']}})
    .then(function success(response){
      //console.log(response);
      vm.datos = response.data;
      if(vm.datos.urlvideo!=""){
        if(vm.datos.urlvideo && vm.datos.urlvideo!=""){
          vm.urlvideo = $sce.trustAsResourceUrl("https://www.youtube.com/embed/"+vm.datos.urlvideo+"?rel=0&vq=large");
          vm.datos.urlvideo = "https://www.youtube.com/watch?v="+vm.datos.urlvideo;
        }
      }
      /*vm.datos.participantes.forEach(function(user, index){
        cargarFoto(user);
      });*/
    },function error(errData){
      console.log("ERROR:"+errData.error);
      console.log(errData.err);
    });

    vm.onSubmit = function(){
      var url = vm.datos.urlvideo.split("?");
      if(url.length>=2){
        url = url[1].split("&");
        for (var i = 0; i < url.length; i++) {
          aux = url[i].split("=");
          if(aux[0]=='v'){//v es el parametro que tiene el id del video
            vm.datos.urlvideo = aux[1];
            break;
          }
        }
      }
      else{//no es una url valida de youtube
        vm.datos.urlvideo = "";
      }
      $http.post("/api/empresa/editar",vm.datos)
      .then(function success(response){
        //console.log(response);
        vm.saved = true;
        $interval(function () {
          vm.saved = false;
          $location.path("muro/"+$routeParams['proyId']);
        }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez

      },function error(errData){
        console.log("Error: "+errData.data.error);
        console.log(errData.data.err);
      });
    };

  };
})();
