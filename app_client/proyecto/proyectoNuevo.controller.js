(function() {

  angular
    .module('meanApp')
    .controller('proyectoNuevoCtrl', proyectoNuevoCtrl);

  proyectoNuevoCtrl.$inject = ['$http','$interval','$location', 'authentication','$routeParams'];
  function proyectoNuevoCtrl($http,$interval,$location, authentication,$routeParams) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var user = authentication.currentUser();
    var proyName = $routeParams['proyName'];

    //console.log("En proyectoNewCtrl proyName es "+proyName);

    //vm.list = [{name:"Facilitador 1",selected:true},{name:"Facilitador 2",selected:false},{name:"Facilitador 3",selected:false}];

    vm.datos = {emprendedor : user._id ,nombre:proyName ,corta:"",larga:"", facilitador:""};
    vm.saved = false;
    vm.onSubmit = function(){
      //console.log('Enviando datos de Proyecto '+ vm.datos.email + " - " + vm.datos.nombre+ " - " +vm.datos.corta + " - "+ vm.datos.larga);
      //console.log("name: "+user.name+" email: "+user.email);
      //console.log("Facilitador : "+vm.datos.facilitador);

      $http.post('/api/proyecto', vm.datos).then(function success(response){
        //$location.path('proyecto');
        //console.log("OK");
        vm.saved = true;
        $interval(function () {
          vm.saved = false;
          $location.path("/proyecto/");
        }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
      }, function error(response){
        console.log("ERROR:"+response.error);
      });
    };

    $http({url:"api/facilitadores",method:"GET"})
      .success(function(data){
        /*for (var variable in data) {
          console.log(variable+"->"+data[variable]);
        }*/
        vm.list = data;
      });
  };

})();
