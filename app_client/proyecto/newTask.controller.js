(function() {

  angular
    .module('meanApp')
    .controller('newTaskCtrl', newTaskCtrl);


//https://ghiden.github.io/angucomplete-alt/ Usar para autocompletar los nombres de los responsables (ver ejemplo 3)
//la idea es que al hacer click a un nombre, se coloque una cajita con el nombre y una x al final para quitarlo
//pero pueda seguir buscando usuarios.

    newTaskCtrl.$inject = ['$http','$routeParams', 'projectService','$interval','$location','authentication','$window'];

    function newTaskCtrl ($http, $routeParams, projectService,$interval,$location, authentication,$window) {
      //console.log("COLID:"+$routeParams['colid']);
      //console.log("TaskLength:"+$routeParams['taskLength']);
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto


      promesa.then(function(proyecto) {
        vm.proy = proyecto;
        vm.proy.hayTareas = false;
        //console.log(vm.proy);
      }, function(reason) {
        vm.proy = null;
      });



      vm.cambio = function(event){//funcion para crear lista de responsables, que se llamará cada vez que se tipee una coma
        //console.log(event);
        if(event.key==","){//presiono una coma
          vm.aux = vm.aux.replace(/,/g,"");
          vm.aux = vm.aux.trim();
          if(vm.aux!="")
            vm.datos.responsables.push(vm.aux);
          vm.aux = "";
        }
      }

      vm.volver = function(){
        window.history.back();
      }
      vm.remove = function(name){
        var pos = vm.datos.responsables.indexOf(name);
        if(pos>-1){
          vm.datos.responsables.splice(pos, 1);
        }
      }
      vm.aux = "";
      vm.resultados = [];
      vm.consultarParticipantes = function(){
        if(vm.aux!=""){
          $http.get("/api/proyecto/queryParticipantes",{params:{consulta:vm.aux}})
          .success(function(response){
            //console.log(response);
            vm.resultados = response;
            /*if(vm.resultados.length==1 && vm.aux==vm.resultados[0]._source.name){
              vm.datos.responsables.push(vm.resultados[0]);
              vm.aux="";
              vm.resultados=[];
            }*/
            for (var i = 0; i < vm.resultados.length; i++) {
              if(vm.aux==vm.resultados[i]._source.name){
                vm.datos.responsables.push(vm.resultados[0]);
                vm.aux="";
                vm.resultados=[];
              }
            }
          })
          .error(function(errData){
            console.log("ERROR:"+errData.error);
            console.log(errData.err);
          });
        }
      }
      vm.onBlur = function(){
        vm.aux = "";
      }
      vm.datos = {nombre:"", descripcion:"",fechaini:"",fechafin:"", responsables:[], columnId:$routeParams['colid'],orden:$routeParams['taskLength'],proyId:$routeParams['proyId']};

      if($routeParams['colid']=='0'){
        vm.datos.nombre = $window.localStorage['mm-titulo-tarea'];
        vm.datos.descripcion = $window.localStorage['mm-descripcion-tarea'];
        $window.localStorage.removeItem('mm-titulo-tarea');
        $window.localStorage.removeItem('mm-descripcion-tarea');
      }
      vm.onSubmit = function(){
        var creador = authentication.currentUser();
        vm.datos.creador = creador._id;

        var fd = new FormData();//necesario para hacer un post
        fd.append('documentos', vm.datos.documentos);
        for (var prop in vm.datos) {
          if (vm.datos.hasOwnProperty(prop)) {
            if(prop=="responsables"){
              fd.append(prop,angular.toJson(vm.datos[prop]));
            }
            else{
              fd.append(prop,vm.datos[prop]);
            }
          }
        }



        //console.log("Enviando:");
        //console.log(fd.get("responsables"));
        $http.post('/api/KanbanTask', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        })
        .then(function success(response){
          //$location.path('proyecto');
          //console.log("OK");
          vm.saved = true;
          $interval(function () {
            vm.saved = false;
            $location.path("kanban/"+$routeParams['proyId']);
          }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
        }, function error(response){
          console.log("ERROR:"+response.data.error);
          console.log(response.data.err);
        });
      };
    }

})();

