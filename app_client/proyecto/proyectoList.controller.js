(function() {

  angular
    .module('meanApp')
    .controller('proyectoListCtrl', proyectoListCtrl);

    proyectoListCtrl.$inject = ['$http', 'authentication','projectService','$location'];
    function proyectoListCtrl($http, authentication,projectService,$location) {
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      vm.currentUser = authentication.currentUser();
      projectService.sinProyecto();

      vm.setId = function(proyId){
        projectService.getProyecto(proyId);
      }

      vm.tieneProyecto = true;

      $http.get("api/proyecto/list", {params: {userId:vm.currentUser._id}})
      .success(function(data){
        console.log(data);
        vm.list = data;
        //console.log(vm.list.length);
        if(vm.list.length>0){
          vm.tieneProyecto = true;

          vm.list.forEach(function(proy, index){
            proy.facilitadores = [];
            proy.emprendedores = [];
            proy.mentores = [];
            proy.participantes.forEach(function(participante, index2){
              cargarFoto(participante);
              if(participante.rol=="emprendedor"){
                proy.emprendedores.push(participante);
              }
              if(participante.rol=="mentor"){
                proy.mentores.push(participante);
              }
              if(participante.rol=="facilitador"){
                proy.facilitadores.push(participante);
              }
            })
          });
        }
        else{
          console.log("ARREGLO DE LARGO 0");
          vm.tieneProyecto = false;
        }
        //console.log(data);
      })
      .error(function(errData){
        console.log("ERROR:"+errData.error);
        vm.tieneProyecto = false;
        vm.mensaje = "Aún no tienes proyectos asociados";
      });

      var cargarFoto = function(usuario){
        $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
        .success(function(response){
          usuario.thumbnail = response.ruta;
        });
      };
    }
})();
