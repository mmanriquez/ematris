(function() {

  angular
    .module('meanApp')
    .controller('newEmpresaCtrl', newEmpresaCtrl);

  newEmpresaCtrl.$inject = ['$http','$interval','$location', 'authentication','$routeParams','projectService'];
  function newEmpresaCtrl($http,$interval,$location, authentication,$routeParams,projectService) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    vm.user = authentication.currentUser();
    var proyId = $routeParams['proyId'];

    var promesa = projectService.getProyecto(proyId);//en $routeParams va el id del proyecto


    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.hayTareas = false;
      //console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
    });


    vm.datos = {proyId:proyId, nombre: "",urlvideo:"",quehace:"",industria:"",estado:"",oportunidad:"",competencia:"",solucion:"",clientes:"",modelo:"",impacto:"", participantes:[]};

    vm.onSubmit = function(){
      var url = vm.datos.urlvideo.split("?");
      if(url.length>=2){
        url = url[1].split("&");
        for (var i = 0; i < url.length; i++) {
          aux = url[i].split("=");
          if(aux[0]=='v'){//v es el parametro que tiene el id del video
            vm.datos.urlvideo = aux[1];
            break;
          }
        }
      }
      else{//no es una url valida de youtube
        vm.datos.urlvideo = "";
      }
      $http.post('/api/empresa', vm.datos).then(function success(response){
        //$location.path('proyecto');
        //console.log("OK");
        vm.saved = true;
        $interval(function () {
          vm.saved = false;
          //console.log(response);
          $location.path("configuracion/"+proyId);
        }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
      }, function error(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });
    };

    vm.volver = function(){
      window.history.back();
    }
  };

})();
