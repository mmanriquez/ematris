(function() {

  angular
    .module('meanApp')
    .controller('muroCtrl', muroCtrl);

  muroCtrl.$inject = ['$http','$routeParams', 'projectService','$location','authentication','$window'];
  function muroCtrl($http,$routeParams, projectService,$location,authentication,$window) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
    vm.proy = {};

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.hayTareas = false;
      console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
    });


    vm.discusiones = [];
    $http.get("api/discusiones",{params:{proyId:$routeParams['proyId']}})
    .then(function(response){
      console.log(response.data);
      vm.discusiones = response.data;
      for (var i in vm.discusiones) {
        if(vm.discusiones[i].likes && vm.discusiones[i].likes.indexOf(vm.user._id)>=0){
          vm.discusiones[i].pintar = true;
        }
        else{
          vm.discusiones[i].pintar = false;
        }
        /*for (var j in vm.discusiones[i].comentarios) {
          cargarFoto(vm.discusiones[i].comentarios[j].autor);
        }*/
      }
    }
    ,function(errData){
      console.log("ERROR:"+errData.error);
      console.log(errData.err);
    });

    vm.user = authentication.currentUser();
    vm.onSubmit = function (discusion) {
      console.log(discusion.nuevoComentario+ " en "+discusion._id);

      var datos = {texto:discusion.nuevoComentario, autor:vm.user._id , discusionId: discusion._id};
      $http.post("api/discusion/comentario", datos)
      .then(function(response){
        console.log(response);
        response.data.autor = vm.user;
        discusion.comentarios.push(response.data);
        discusion.nuevoComentario = "";
      },
      function(errData){
        console.log("ERROR: "+errData.error);
        console.log(errData.err);
      });
    }

    vm.meGustaDiscusion = function(discussion){
      if(!discussion.likes){
        discussion.likes = [];
      }
      var datos = {discussionId: discussion._id, userId: vm.user._id};
      var pos = discussion.likes.indexOf(vm.user._id);
      if(pos==-1){
        $http.post('api/like/discussion',datos)
        .then(function(response){
          if(discussion.likes){
            discussion.likes.push(vm.user._id);
            discussion.pintar = true;
          }
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
      else{
        $http.post('api/unlike/discussion',datos)
        .then(function(response){
          if(discussion.likes){
            discussion.likes.splice(pos,1);
            discussion.pintar = false;
          }
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    }

    vm.enviarTarea = function(texto,titulo){
      titulo = titulo || "";
      console.log(titulo);
      console.log(texto);
      $window.localStorage.setItem('mm-titulo-tarea', titulo);
      $window.localStorage.setItem('mm-descripcion-tarea', texto);
      $location.path("/new/task/0/"+$routeParams['proyId']);
    };



    var cargarFoto = function(usuario){
      $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.thumbnail = response.ruta;
      });
    };
    cargarFoto(vm.user);
  };
})();
