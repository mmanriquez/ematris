(function() {

  angular
    .module('meanApp')
    .controller('reunionesCtrl', reunionesCtrl);

    reunionesCtrl.$inject = ['projectService','$routeParams','$http','authentication','$location'];
    function reunionesCtrl (projectService,$routeParams,$http,authentication,$location) {
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      //console.log('Reunion!! :D');
      var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
      vm.proy = {};

      promesa.then(function(proyecto) {
          vm.proy = proyecto;
        }, function(reason) {
          vm.proy = null;
        });

      $http({url: "api/reuniones", method: "GET",params: {proyId: $routeParams["proyId"]}})
      .success(function(data){
        //console.log(data);
        vm.reuniones = data.reuniones;
        vm.meses = [{mes:"Enero",reuniones:[]},
                    {mes:"Febrero",reuniones:[]},
                    {mes:"Marzo",reuniones:[]},
                    {mes:"Abril",reuniones:[]},
                    {mes:"Mayo",reuniones:[]},
                    {mes:"Junio",reuniones:[]},
                    {mes:"Julio",reuniones:[]},
                    {mes:"Agosto",reuniones:[]},
                    {mes:"Septiembre",reuniones:[]},
                    {mes:"Octubre",reuniones:[]},
                    {mes:"Noviembre",reuniones:[]},
                    {mes:"Diciembre",reuniones:[]}
        ];
        for (var i = 0; i < vm.reuniones.length; i++) {
          vm.reuniones[i].fecha = new Date(vm.reuniones[i].fecha);
          vm.reuniones[i].horaini = new Date(vm.reuniones[i].horaini);
          var y = vm.reuniones[i].fecha.getFullYear();
          var m = vm.reuniones[i].fecha.getMonth();
          var d = vm.reuniones[i].fecha.getDate();
          var hoy = new Date();
          if(y==hoy.getFullYear() && m==hoy.getMonth() && d==hoy.getDate()){
            vm.reuniones[i].hoy = true;
          }
          else{
            vm.reuniones[i].hoy = false;
          }
          if(hoy>vm.reuniones[i].horaini){
            vm.reuniones[i].paso = true;
          }
          else{
            vm.reuniones[i].paso = false;
          }
        }
        vm.reuniones.sort(function(a,b){return a.horaini - b.horaini});
        for (var i = 0; i < vm.reuniones.length; i++) {
          vm.meses[vm.reuniones[i].fecha.getMonth()].reuniones.push(vm.reuniones[i]);
        }
        //console.log("Cargo bien las reuniones.");
      })
      .error(function(errData){
        console.log("ERROR: Al solicitar columnas del proyecto.");
        console.log(errData);
      });
    }

})();
