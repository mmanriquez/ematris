(function() {

  angular
    .module('meanApp')
    .controller('participantsCtrl', participantsCtrl);

    participantsCtrl.$inject = ['projectService','$routeParams','$http','$interval','$location','authentication'];
    function participantsCtrl (projectService,$routeParams,$http,$interval,$location,authentication) {
      //console.log('participantes!');
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);
     // var promesa2 = projectService.getRecomendados($routeParams['proyId']);//en $routeParams va el id del proyecto

      promesa.then(function(proyecto) {
          vm.proy = proyecto;
          //console.log("Cargo bien el proyecto");
          //console.log(vm.proy);
        }, function(reason) {
          vm.proy = null;
          console.log("ERROR: Al cargar el proyecto");
        }
      );

      //console.log("Ya tengo el proyecto ------------------");

      vm.volver = function(){
        window.history.back();
      }

      vm.participantes = [];
      $http({url: "api/proyecto/getParticipantes", method: "GET",params: {proyId: $routeParams["proyId"]}})
      .success(function(data){
            //console.log("Cargo bien los participantes.");
            vm.participantes = data;
            //console.log(vm.participantes);

            vm.mentores = [];
            $http({url: "api/proyecto/getRecomendados", method: "GET",params: {proyId: $routeParams["proyId"]}})
            .success(function(data2){
              //console.log("Cargo bien las recomendados.");
              vm.mentores = data2;
              for (var j = vm.participantes.length - 1; j >= 0; j--) {
                for (var i = vm.mentores.length - 1; i >= 0; i--) {
                  if(vm.participantes.length != 0){
                    if(vm.mentores[i]._id == vm.participantes[j]._id){
                      vm.mentores.splice([i], 1);
                     }
                  }
                }
              }
            })
            .error(function(errData2){
              console.log("ERROR: Al solicitar recomendados.");
              console.log(errData2);
            });
      })
      .error(function(errData){
        console.log("ERROR: Al solicitar participantes.");
        console.log(errData);
      });





      vm.mentoresSeleccionadas = [];
      vm.toggle = function(idmentor){
        var pos=-1;
        if((pos = vm.mentoresSeleccionadas.indexOf(idmentor))>=0){
          vm.mentoresSeleccionadas.splice(pos, 1);
        }
        else{
          vm.mentoresSeleccionadas.push(idmentor);
        }
      }



      vm.datos = {proyId: $routeParams["proyId"], mentores:vm.mentoresSeleccionadas};
      vm.saved = false;
      vm.onSubmit = function(){
        //console.log(vm.datos);
        //console.log("Llamando a api/poryecto/mentor");
        $http.post('/api/proyecto/mentor', vm.datos).then(function success(response){
          //$location.path('proyecto');
          //console.log("OK");
          vm.saved = true;
          $interval(function () {
            vm.saved = false;
            //console.log(response);
            $location.path("/project/"+$routeParams["proyId"]);
          }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
        }, function error(response){
          console.log(response);
          console.log("ERROR:"+response.data.error);
        });

      }
    }


})();
