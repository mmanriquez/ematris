(function() {

  angular
    .module('meanApp')
    .controller('hitoNewCtrl', hitoNewCtrl);

    hitoNewCtrl.$inject = ['projectService','$routeParams','$http','$interval','$location','authentication'];
    function hitoNewCtrl (projectService,$routeParams,$http,$interval,$location,authentication) {
      //console.log('Nuevo hito!');
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto

      promesa.then(function(proyecto) {
          vm.proy = proyecto;
          //console.log("Cargo bien el proyecto");
          //console.log(vm.proy);
        }, function(reason) {
          vm.proy = null;
          console.log("ERROR: Al cargar el proyecto");
        }
      );

      vm.volver = function(){
        window.history.back();
      }

      vm.tareasSinHito = [];
      $http({url: "api/tareasSinHito", method: "GET",params: {proyId: $routeParams["proyId"]}})
          .success(function(data){
            //console.log("Cargo bien las tareas.");
            //console.log(data);
            for (var i = 0; i < data.length; i++) {
                data[i].fecha = new Date(data[i].fecha);
            }
            data.sort(function(a, b){return a.fecha - b.fecha});
            vm.tareasSinHito = data;
          })
          .error(function(errData){
            console.log("ERROR: Al solicitar columnas del proyecto.");
            console.log(errData);
          });

      vm.tareasSeleccionadas = [];
      vm.toggle = function(idTarea){
        var pos=-1;
        if((pos = vm.tareasSeleccionadas.indexOf(idTarea))>=0){
          vm.tareasSeleccionadas.splice(pos, 1);
        }
        else{
          vm.tareasSeleccionadas.push(idTarea);
        }
      }

      vm.datos = {proyId: $routeParams["proyId"], nombre : "", tareas:vm.tareasSeleccionadas, numHitos: $routeParams["numHitos"]};
      vm.saved = false;
      vm.onSubmit = function(){
        console.log(vm.datos);
        $http.post('/api/hito', vm.datos).then(function success(response){
          //$location.path('proyecto');
          console.log("OK");
          vm.saved = true;
          $interval(function () {
            vm.saved = false;
            console.log(response);
            $location.path("/ruta/"+$routeParams["proyId"]);
          }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
        }, function error(response){
          console.log(response);
          console.log("ERROR:"+response.data.error);
        });

      }
    }


})();
