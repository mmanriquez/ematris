(function() {

  angular
    .module('meanApp')
    .controller('taskCtrl', taskCtrl);


    taskCtrl.$inject = ['projectService','$routeParams','$http', 'authentication','$interval','$location'];

    function taskCtrl (projectService,$routeParams,$http, authentication,$interval,$location) {
      //console.log('Tarea!!!!');
      //console.log($routeParams);
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);
      promesa.then(function(proyecto) {
        vm.proy = proyecto;
        /*$http({url:"api/columna", method: "GET", params:{colId: $routeParams['colId']}})
        .success(function(data){
          vm.columna = data;
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });*/


      }, function(reason) {
        vm.proy = null;
        console.log("ERROR: Al cargar el proyecto");
      });

      var getTarea = function(){
        $http({url: "api/tarea", method: "GET",params: {taskId:$routeParams['taskId'],proyId:$routeParams['proyId']}})
        .success(function(data){
          //console.log(data);
          vm.tarea = data;
          for (var i = 0; i < vm.tarea.documentos.length; i++) {
            vm.tarea.documentos[i].url = "documents/"+vm.tarea.documentos[i]._id+"."+vm.tarea.documentos[i].nombre.split(".")[1];
          }
          for (var i = 0; i < vm.tarea.responsables.length; i++) {
            cargarFoto(vm.tarea.responsables[i]);
          }
          cargarFoto(vm.tarea.creador);
          vm.tarea.comentarios.sort(function(a, b){
            var fechaa = new Date(a.fecha);
            var fechab = new Date(b.fecha);
            return fechaa-fechab;
          });

          for (var i = 0; i < vm.tarea.comentarios.length; i++) {
            cargarFoto(vm.tarea.comentarios[i].autor);
          }
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
      getTarea();

      vm.comentario = "";
      vm.user = authentication.currentUser();
      vm.onComment = function(){
        var datos = {userId : vm.user._id, comentario:vm.comentario, taskId: $routeParams['taskId']};
        //console.log(datos);
        $http.post("/api/comentario",datos)
        .then(function(data){
          data.data.autor = vm.user;
          vm.tarea.comentarios.push(data.data);
          vm.comentario = "";
        },function(errData){
          console.log("ERROR:"+errData.error);
        });
      }

      vm.eliminarTarea = function(){
        $http.delete("/api/KanbanTask",{params:{taskId: $routeParams['taskId'] ,proyId:$routeParams['proyId']}})
        .success(function(response){
          vm.eliminada = true;
          $interval(function () {
            vm.eliminada = false;
            $location.path("kanban/"+$routeParams['proyId']);
          }, 2000, 1);
        })
        .error(function(errData){
          vm.mensajeError = errData.error;
          vm.mostrarError = true;
          $interval(function () {
            vm.mostrarError = false;
          }, 2000, 1);
        })
      };


      var cargarFoto = function(usuario){
        $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
        .success(function(response){
          usuario.thumbnail = response.ruta;
        });
      };
      cargarFoto(vm.user);
    }
})();
