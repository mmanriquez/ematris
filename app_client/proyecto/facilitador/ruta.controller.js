(function() {

  angular
    .module('meanApp')
    .controller('rutaCtrl', rutaCtrl);


    rutaCtrl.$inject = ['projectService','$routeParams','$http','authentication','$location'];

    function rutaCtrl (projectService,$routeParams,$http,authentication,$location) {
      //console.log('Ruta!!!!');
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);
      promesa.then(function(proyecto) {
          vm.proy = proyecto;
          if((typeof proyecto.hitos === undefined) || proyecto.hitos == null ){
            vm.numHitos = 0;
          }
          else{
            vm.numHitos = proyecto.hitos.length;
          }
          //console.log(vm.numHitos);
          //console.log("Cargo bien el proyecto");
          vm.hitos = [];
          //esta url esta siendo tomada por angular y como no existe me redirecciona al home
          $http({url: "api/hitos", method: "GET",params: {hitos:vm.proy.hitos}})//en route params va el id del usuario
          .success(function(data){
            //console.log(data);
            vm.hitos = data;
          })
          .error(function(errData){
            console.log("ERROR:"+errData);
          });
          //console.log(vm.proy);
        }, function(reason) {
          vm.proy = null;
          console.log("ERROR: Al cargar el proyecto");
        });

      vm.nuevoHito = function() {
        console.log("Nuevo hitoo!!! :D");
      }
    }

})();
