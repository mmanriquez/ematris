(function() {

  angular
    .module('meanApp')
    .controller('reunionNewCtrl', reunionNewCtrl);

    reunionNewCtrl.$inject = ['projectService','$routeParams','authentication','$http','$interval','$location'];
    function reunionNewCtrl (projectService,$routeParams,authentication,$http,$interval,$location) {
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
      vm.proy = {};


      promesa.then(function(proyecto) {
          vm.proy = proyecto;
        }, function(reason) {
          vm.proy = null;
        }
      )

      vm.cambio = function(event){//funcion para crear lista de responsables, que se llamará cada vez que se tipee una coma
        //console.log(event);
        if(event.key==","){//presiono una coma
          vm.aux = vm.aux.replace(",","");
          vm.datos.participantes.push(vm.aux);
          vm.aux = "";
        }
      }

      vm.volver = function(){
        window.history.back();
      }
      vm.remove = function(name){
        var pos = vm.datos.participantes.indexOf(name);
        if(pos>-1){
          vm.datos.participantes.splice(pos, 1);
        }
      }

      vm.onBlur = function(){
        vm.aux = vm.aux.trim();
        if(vm.aux!=""){
          vm.datos.participantes.push(vm.aux);
          vm.aux = "";
        }
      };
      vm.aux = "";
      var creador = authentication.currentUser();
      vm.datos = {nombre:"", creador:creador._id,objetivos:"", fecha:"", horaini:"", horafin:"",lugar:"",participantes:[],proyId:$routeParams['proyId']};
      /*reunion.nombre = req.body.nombre;
      reunion.creador = req.body.creador;
      reunion.objetivos = req.body.objetivos;
      reunion.fecha = req.body.fecha;
      reunion.horaini = req.body.horaini;
      reunion.horafin = req.body.horafin;
      reunion.lugar = req.body.lugar;
      reunion.participantes = req.body.participantes;*/
      vm.onSubmit = function(){
        //console.log(vm.datos);
        $http.post('/api/reunion', vm.datos).then(function success(response){
          //$location.path('proyecto');
          //console.log("OK");
          vm.saved = true;
          $interval(function () {
            vm.saved = false;
            $location.path("reuniones/"+$routeParams['proyId']);
          }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
        }, function error(response){
          console.log("ERROR:"+response.data.error);
          console.log(response.data.err);
        });
      };
    }

})();
