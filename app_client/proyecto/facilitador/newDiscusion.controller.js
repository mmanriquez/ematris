(function() {

  angular
    .module('meanApp')
    .controller('newDiscusionCtrl', newDiscusionCtrl);

    newDiscusionCtrl.$inject = ['$routeParams','$http','$interval','$location','authentication'];
    function newDiscusionCtrl ($routeParams,$http,$interval,$location,authentication) {
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }

      vm.volver = function(){
        window.history.back();
      }
      var autor = authentication.currentUser();
      vm.datos = {titulo:"",discusion:"",autor : autor._id, proyId:$routeParams['proyId']};
      vm.onSubmit = function(){
        //console.log(vm.datos);
        $http.post('/api/discusion', vm.datos)
        .then(function success(response){
          //console.log("OK");
          vm.saved = true;
          $interval(function () {
            vm.saved = false;
            //console.log(response);
            $location.path("/muro/"+$routeParams["proyId"]);
          }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
        }, function error(response){
          console.log(response);
          console.log("ERROR:"+response.data.error);
        });
      }

    }

})();
