(function() {

  angular
    .module('meanApp')
    .controller('kanbanCtrl', kanbanCtrl);

  kanbanCtrl.$inject = ['$http','kanbanService','$routeParams','projectService','$location','$interval','authentication'];
  function kanbanCtrl($http,kanbanService,$routeParams,projectService,$location,$interval,authentication) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }

    console.log("En kanban!!!!");
    console.log("Viendo :"+$routeParams['proyId']);

    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      console.log("Cargo bien el proyecto");
      //console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
      console.log("ERROR: Al cargar el proyecto");
    });

    var cargarFoto = function(usuario){
      $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.thumbnail = response.ruta;
      });
    };

    vm.call = function(srcCol, srcIndex, targetCol, targetIndex){
      var aux = targetCol.tareas.length + 1;
      console.log("WIP:"+targetCol.wip);
      console.log("LEN:"+aux);
      console.log(parseInt(targetCol.wip)>=parseInt(aux));
      // Copy the item from source to target.
      //if(parseInt(targetCol.wip)==0 || parseInt(targetCol.wip)>=parseInt(aux) || targetCol._id == srcCol._id){

        targetCol.tareas.splice(targetIndex, 0, srcCol.tareas[srcIndex]);
        // Remove the item from the source, possibly correcting the index first.
        // We must do this immediately, otherwise ng-repeat complains about duplicates.
        if (srcCol.tareas == targetCol.tareas && targetIndex <= srcIndex) srcIndex++;
        srcCol.tareas.splice(srcIndex, 1);
        // By returning true from dnd-drop we signalize we already inserted the item.

        for (var i = 0; i < targetCol.tareas.length; i++) {
          targetCol.tareas[i].orden = i;
        }

        for (var i = 0; i < srcCol.tareas.length; i++) {
          srcCol.tareas[i].orden = i
        }

        //lanzar un post a la API para guardar el estado de las columnas
        //put("api/setKanbanTasks",{origen: srcList, destino:targetCol.tareas});
        vm.saving = true;
        $http.put('/api/setKanbanTasks', {origen: srcCol , destino:targetCol}).then(function success(response){
          //$location.path('proyecto');
          console.log("OK: Tareas guardadas");
          vm.saving = false;
        }, function error(response){
          vm.saving = false;
          console.log("ERROR:Al guardar cambios de las tareas");
        });


        //mantener una variable que impida cambiar de página mientras se esté realizando el proceso de guardado
        //y avisar cuando se quiere abandonar la página mientras se este guardando.


        return true;
      //} //remuevo el if para que siempre se pueda mover una tarea de una columna a otra, independiente de la cantidad de tareas en la columna
      //return false;
    }

    vm.createTask = function(col){
      /*var largo = col.tareas.length;
      if(largo<col.wip || col.wip==0){
        $location.path("new/task/"+col._id+"/"+$routeParams['proyId']);
      }
      else{
        alert("No se puede crear una nueva tarea en esta columna");
      }*/
      $location.path("new/task/"+col._id+"/"+$routeParams['proyId']);

    };

    vm.update = function(columna){
      if(columna.cambio){
        $http.put("/api/setKanbanColumn",{colid:columna._id,nombre:columna.nombre})
        .success(function(response){
          vm.actualizado = true;
          $interval(function () {
            vm.actualizado = false;
          }, 2000, 1);
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    }

    vm.isLoading = true;
    //kanbanService.getColumns($routeParams["proyId"]);
    $http({url: "api/KanbanBoard", method: "GET",params: {proyId: $routeParams["proyId"]}})
    .success(function(data){
      console.log(data);
      console.log("Cargo bien las columnas.");
      vm.columns = data;
      for (var i = 0; i < vm.columns.length; i++) {
        vm.columns[i].tareas.sort(function(a,b){return a.orden - b.orden});
        for (var j = 0; j < vm.columns[i].tareas.length; j++) {
          vm.columns[i].tareas[j].responsables.forEach(function(resp, index){
            cargarFoto(resp);
          });
        }
      }
    })
    .error(function(errData){
      console.log("ERROR: Al solicitar columnas del proyecto.");
      console.log(errData);
    });
  }
})();
