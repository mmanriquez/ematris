(function() {

  angular
    .module('meanApp')
    .controller('projectNew2Ctrl', projectNew2Ctrl);

  projectNew2Ctrl.$inject = ['$http','$interval','$location', 'authentication','$routeParams','$window'];
  function projectNew2Ctrl($http,$interval,$location, authentication,$routeParams,$window) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var user = authentication.currentUser();
    vm.datos = JSON.parse($window.localStorage['mm-nuevoProyecto']);

    //console.log(vm.datos);

    vm.volver = function(){
      window.history.back();
    };

    console.log(vm.datos.palabras);
    $http.get("/api/proyecto/queryMentores",{params:{palabras:vm.datos.palabras}})
    .then(function(response){
      console.log(response.data);
      vm.recomendados = response.data.hits;
      var scoreTotal = 0;
      if(vm.recomendados.length==0){
        vm.mostrarAlerta = true;
      }
      for (var i = 0; i < vm.recomendados.length; i++) {
        scoreTotal += vm.recomendados[i]._score;
      }
      vm.recomendados.forEach(function(mentor){
        cargarFoto(mentor);
        mentor.cliqueado = false;
        mentor.porcentaje = parseFloat((mentor._score / scoreTotal )*100).toFixed(2);
      });
    },function(errData){
      console.log("ERROR:"+errData.data.error);
      console.log(errData);
    });

    vm.onSubmit = function(){
      //console.log('Enviando datos de Proyecto '+ vm.datos.email + " - " + vm.datos.nombre+ " - " +vm.datos.corta + " - "+ vm.datos.larga);
      //console.log("name: "+user.name+" email: "+user.email);
      //console.log("Facilitador : "+vm.datos.facilitador);
      vm.datos.mentores = [];
      if(vm.recomendados){
        for (var i = 0; i < vm.recomendados.length; i++) {
          if(vm.recomendados[i].cliqueado){
            vm.datos.mentores.push(vm.recomendados[i]);
          }
        }
      }
      $window.localStorage['mm-nuevoProyecto'] = JSON.stringify(vm.datos);
      $location.path('/new/project/3/');
      /*
      if(vm.participantes.length==0){
        vm.mensaje = "Debe elegir algun participante";
        return;
      }
      vm.datos.participantes = vm.participantes;
      $http.post("/api/proyecto",vm.datos)
      .then(function success(response){
        vm.saved = true;
        $interval(function () {
          vm.saved = false;
          console.log(response);
          $location.path("muro/"+response.data._id);
        }, 2000, 1);//luego de 2 segundos se llama a esta funcion, y se repite solo una vez
      },function error(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });*/
    }

    var cargarFoto = function(usuario){
      $http.get("/api/userImage", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.imagen = response.ruta;
      });
    };

  };

})();
