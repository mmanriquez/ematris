(function() {

  angular
    .module('meanApp')
    .controller('editTaskCtrl', editTaskCtrl);

    editTaskCtrl.$inject = ['projectService','$routeParams','$http', 'authentication','$filter','$location'];

    function editTaskCtrl(projectService,$routeParams,$http, authentication,$filter,$location){
      var vm = this;
      if(!authentication.isLoggedIn()){
        $location.path('/')
      }
      var promesa = projectService.getProyecto($routeParams['proyId']);
      promesa.then(function(proyecto) {
        vm.proy = proyecto;
        $http({url: "api/tarea", method: "GET",params: {proyId:$routeParams['proyId'],taskId:$routeParams['taskId']}})
        .success(function(data){
          //console.log(data);
          vm.datos = data;
          vm.datos.fechaini = $filter('date')(vm.datos.fechaini, "dd/MM/yyyy");
          vm.datos.fechafin = $filter('date')(vm.datos.fechafin, "dd/MM/yyyy");
          /*vm.datos.comentarios.sort(function(a, b){
            var fechaa = new Date(a.fecha);
            var fechab = new Date(b.fecha);
            return fechaa-fechab;
          });*/
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });

      }, function(errData) {
        vm.proy = null;
        console.log("ERROR: Al cargar el proyecto");
      });


      vm.volver = function(){
        window.history.back();
      };

      vm.aux = "";
      vm.resultados = [];
      vm.consultarParticipantes = function(){
        if(vm.aux!=""){
          $http.get("/api/proyecto/queryParticipantes",{params:{consulta:vm.aux}})
          .success(function(response){
            //console.log(response);
            vm.resultados = response;
            if(vm.resultados.length==1 && vm.aux==vm.resultados[0]._source.name){
              var usuarioTemporal = {_id:vm.resultados[0]._id,name:vm.resultados[0]._source.name};
              vm.datos.responsables.push(usuarioTemporal);
              vm.aux="";
              vm.resultados=[];
            }
          })
          .error(function(errData){
            console.log("ERROR:"+errData.error);
            console.log(errData.err);
          });
        }
      }
      vm.onBlur = function(){
        vm.aux = "";
      }
      vm.remove = function(name){
        var pos = vm.datos.responsables.indexOf(name);
        if(pos>-1){
          vm.datos.responsables.splice(pos, 1);
        }
      }
      var user = authentication.currentUser();

      vm.actualizar = function(){
        var tarea = {nombre:vm.datos.nombre, descripcion:vm.datos.descripcion,fechaini:vm.datos.fechaini,fechafin:vm.datos.fechafin, responsables:vm.datos.responsables,actualizador:user._id};
        $http.post("/api/setKanbanTask",tarea)
        .success(function(){

        });//LLAMAR A SERVICIO QUE ACTUALIZA UNA TAREA!!!!!!!!!!!!!!
      }
    }

})();
