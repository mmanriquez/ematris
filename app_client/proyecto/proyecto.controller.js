//controlador de la vista de un proyecto desde el punto de vista de un EMPRENDEDOR
(function() {

  angular
    .module('meanApp')
    .controller('proyectoCtrl', proyectoCtrl);

  proyectoCtrl.$inject = ['$http','$routeParams', 'authentication','projectService','$location'];
  function proyectoCtrl($http,$routeParams, authentication,projectService,$location) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    //vm.currentUser = authentication.currentUser();
    //console.log("Parametros");



    vm.proy = {nombre:"",descripcionCorta:"",descripcionCompleta:""};
    $http({url: "api/proyecto", method: "GET",params: {userId: $routeParams['userId']}})//en route params va el id del usuario
    .success(function(data){
      vm.proy = data.proyecto;
      /*for (var variable in vm.proy) {
        console.log("proyecto.controller: "+variable+"->"+vm.proy[variable])
      }*/
      vm.sinProyecto = false;

      $http({url: "api/KanbanBoard", method: "GET",params: {proyId: vm.proy._id}})
      .success(function(data){
        //console.log(data);
        //console.log("Cargo bien las columnas.");
        vm.tasks = [];
        for (var col in data) {
          for (var i = 0; i < data[col].tareas.length; i++) {
            var tarea = data[col].tareas[i];
            tarea.fechaCreacion = new Date(tarea.fechaCreacion);
            tarea.colid = data[col]._id;
            vm.tasks.push(tarea);
          }
        }
        vm.tasks.sort(function(a, b){return a.fechaCreacion - b.fechaCreacion});
      })
      .error(function(errData){
        console.log("ERROR: Al solicitar columnas del proyecto.");
        console.log(errData);
      });
    })
    .error(function(errData){
      console.log("ERROR: Al recuperar el proyecto");
      console.log(errData);
      vm.sinProyecto = true;
    });



  };
})();
