$(function(){
  var fixHeight = function(){
    altura = $("#contenido").height();
    console.log(altura);
    $(".side-menu-background").height(altura) ;
  };
  fixHeight();
});
(function() {
  angular
    .module('meanApp')
    .controller('discusionCtrl', discusionCtrl);

  discusionCtrl.$inject = ['$http','$routeParams', 'projectService','$location','authentication','$window'];
  function discusionCtrl($http,$routeParams, projectService,$location,authentication,$window) {
    var fixHeight = function(){
      altura = $("#contenido").height();
      //console.log(altura);
      $(".side-menu-background").height(altura) ;
    };

    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto
    vm.proy = {};

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.hayTareas = false;
      //console.log(vm.proy);
    }, function(reason) {
      vm.proy = null;
    });



    vm.discusiones = [];
    $http.get("api/discusiones",{params:{proyId:$routeParams['proyId']}})
    .then(function(response){
      //console.log(response.data);
      vm.discusiones = response.data;
    }
    ,function(errData){
      console.log("ERROR:"+errData.error);
      console.log(errData.err);
    });

    vm.discusion = {};
    vm.destacados = [];
    //console.log("consultando "+$routeParams["discusionId"]);
    $http.get("/api/discusion",{params:{discusionId:$routeParams["discusionId"]}})
    .success(function(response){
      //console.log(response);
      vm.discusion = response;
      if(vm.discusion.likes && vm.discusion.likes.indexOf(vm.user._id)>=0){
        vm.discusion.pintar = true;
      }
      else{
        vm.discusion.pintar = false;
      }
      for (var i = 0; i < vm.discusion.comentarios.length; i++) {
        cargarFoto(vm.discusion.comentarios[i].autor);
        if(vm.discusion.comentarios[i].hallazgo){
          vm.destacados.push(vm.discusion.comentarios[i]);
        }
        if(vm.discusion.comentarios[i].likes && vm.discusion.comentarios[i].likes.indexOf(vm.user._id)>=0){
          vm.discusion.comentarios[i].pintar = true;
        }
        else{
          vm.discusion.comentarios[i].pintar = false;
        }
      }
      fixHeight();
    })
    .error(function(errData){
      console.log("ERROR:"+errData.error);
      console.log(errData.err);
    });

    var cargarFoto = function(usuario){
      $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.thumbnail = response.ruta;
      });
    };
    vm.user = authentication.currentUser();
    cargarFoto(vm.user);

    vm.onSubmit = function (discusion) {
      //console.log(discusion.nuevoComentario+ " en "+discusion._id);

      var datos = {texto:discusion.nuevoComentario, autor:vm.user._id , discusionId: discusion._id};
      $http.post("api/discusion/comentario", datos)
      .then(function(response){
        //console.log(response);
        response.data.autor = vm.user;
        discusion.comentarios.push(response.data);
        discusion.nuevoComentario = "";
      },
      function(errData){
        console.log("ERROR: "+errData.error);
        console.log(errData.err);
      });
    }
    vm.addTakeaway = function(comment){
      if(!comment.hallazgo){
        //console.log(comment);
        comment.hallazgo = true;
        vm.destacados.push(comment);
        var datos = {comment:comment, projectId:$routeParams['proyId']}
        $http.post('api/discusion/takeaway', datos)
        .then(function(response){
          //console.log('response',response)
        }, function(err){
          console.log('err', err);
        });
      }
      else{
        //comment.hallazgo = false;

        modificarEnDB(comment);
      }
    };


    vm.meGustaDiscusion = function(discussion){
      if(!discussion.likes){
        discussion.likes = [];
      }
      var datos = {discussionId: discussion._id, userId: vm.user._id};
      var pos = discussion.likes.indexOf(vm.user._id);
      if(pos==-1){
        $http.post('api/like/discussion',datos)
        .then(function(response){
          if(discussion.likes){
            discussion.likes.push(vm.user._id);
            discussion.pintar = true;
          }
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
      else{
        $http.post('api/unlike/discussion',datos)
        .then(function(response){
          if(discussion.likes){
            discussion.likes.splice(pos,1);
            discussion.pintar = false;
          }
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    }
    vm.meGusta = function(comment){
      if(!comment.likes){
        comment.likes = [];
      }
      var datos = {commentId:comment._id , userId:vm.user._id};
      var posComentario = comment.likes.indexOf(vm.user._id);
      if(posComentario==-1){
        //console.log("Al usuario "+datos.userId+" le gusta el comentario "+datos.commentId);
        $http.post('api/like/comentario',datos)
        .then(function(response){
          if(comment.likes){
            comment.likes.push(vm.user._id);
            comment.pintar = true;
          }
          //console.log(response);
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
      else{
        $http.post('api/unlike/comentario',datos)
        .then(function(response){
          if(comment.likes){
            comment.likes.splice(posComentario,1);
            comment.pintar = false;
          }
          //console.log(response);
        }, function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    };

    vm.quitar = function(pos){
      //console.log("quitando "+pos);
      //quitados = vm.destacados.splice(pos,1);
      modificarEnDB(vm.destacados[pos]);
    }

    vm.enviarTarea = function(texto,titulo){
      titulo = titulo || "";
      //console.log(titulo);
      //console.log(texto);
      $window.localStorage.setItem('mm-titulo-tarea', titulo);
      $window.localStorage.setItem('mm-descripcion-tarea', texto);
      $location.path("/new/task/0/"+$routeParams['proyId']);
    };

    var modificarEnDB = function(comment){
      $http.delete("api/discusion/takeaway",{params:{commentId:comment._id}})
      .success(function(response){
        vm.destacados.splice(vm.destacados.indexOf(comment),1);
        vm.discusion.comentarios[vm.discusion.comentarios.indexOf(comment)].hallazgo = false;
      })
      .error(function(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });
    };
  };

})();
