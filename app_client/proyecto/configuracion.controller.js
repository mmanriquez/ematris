(function() {

  angular
    .module('meanApp')
    .controller('configCtrl', configCtrl);

  configCtrl.$inject = ['$http','$routeParams', 'projectService','$location','authentication','$interval','$window'];
  function configCtrl($http,$routeParams, projectService,$location,authentication,$interval,$window) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    vm.proy = {};
    var promesa = projectService.getProyecto($routeParams['proyId']);//en $routeParams va el id del proyecto

    promesa.then(function(proyecto) {
      vm.proy = proyecto;
      vm.proy.participantes.forEach(function(user, index){
        cargarThumbnail(user);
      });
      $http.get("/api/columnas",{params: {proyId: $routeParams["proyId"]}})
      .success(function(data){
        //console.log(data);
        //console.log("Cargo bien las columnas.");
        vm.proy.columnas = data;
      })
      .error(function(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });
    }, function(reason) {
      vm.proy = null;
    });

    vm.user = authentication.currentUser();


    vm.consulta = "";
    vm.resultados = [];
    //vm.proy.participantes = [];
    vm.consultarParticipantes = function(){
      if(vm.consulta!=""){
        $http.get("/api/proyecto/queryParticipantes",{params:{consulta:vm.consulta}})
        .success(function(response){
          //console.log(response);
          vm.resultados = response;
          if(vm.resultados.length==1 && vm.consulta==vm.resultados[0]._source.name){
            var pos = buscarParticipante(vm.resultados[0], vm.proy.participantes);
            if(pos==-1){
              vm.proy.participantes.push(vm.resultados[0]);
              cargarThumbnail(vm.proy.participantes[vm.proy.participantes.length - 1]);
              vm.consulta="";
              vm.resultados=[];
            }
            else{
              vm.proy.participantes[pos].resaltar = true;
              vm.consulta = "";
              vm.resultados = [];
              $interval(function () {
                vm.proy.participantes[pos].resaltar = false;
              }, 2000, 1);
            }
          }
        })
        .error(function(errData){
          console.log("ERROR:"+errData.error);
          console.log(errData.err);
        });
      }
    }

    vm.saveNewColumna = function(){
      //console.log("En saveNewColumna");
      if(vm.nuevaColumna!=""){
        $http.post("/api/KanbanColumn",{proyId:vm.proy._id, nombre : vm.nuevaColumna})
        .success(function(response){
          vm.proy.columnas.push(response);
          vm.showNew = false;
          vm.nuevaColumna = "";
        })
        .error(function(errData){
          console.log("ERROR"+errData.error);
          console.log(errData.err);
        });
      }
    };

    vm.eliminarColumna = function(col){
      vm.colSeleccionada = col;
      vm.preguntarColumna = true;
    }

    vm.onSubmit = function(){
      var datos = {
        nombre : vm.proy.nombre,
        descripcionCorta : vm.proy.descripcionCorta,
        descripcionCompleta : vm.proy.descripcionCompleta,
        palabras : vm.proy.palabras,
        empresas : [],
        participantes : []
      };

      for (var i = 0; i < vm.proy.participantes.length; i++) {
        datos.participantes.push(vm.proy.participantes[i]._id);
      }

      for (var i = 0; i < vm.proy.empresas.length; i++) {
        datos.empresas.push(vm.proy.empresas[i]._id);
      }

      $http.post("api/proyecto/actualizar",{proyId: $routeParams['proyId'],datos:datos})
      .success(function(response){
        vm.actualizado = true;
        $interval(function () {
          vm.actualizado = false;
        }, 2000, 1);
      })
      .error(function(errData){
        vm.error = true;
        $interval(function () {
          vm.error = false;
        }, 2000, 1);
      });
    };

    vm.modal = function(user){
      vm.seleccionado = user;
      cargarFoto(vm.seleccionado);
      vm.mostrar = true;
    }

    vm.modalEmpresa = function(empresa){
      vm.seleccionada = empresa;
      vm.preguntarEmpresa = true;
    }

    vm.remove = function(user){
      var pos = vm.proy.participantes.indexOf(user);
      if(pos>-1){
        vm.proy.participantes.splice(pos, 1);
        vm.mostrar = false;
      }
    }

    vm.removeEmpresa = function(empresa){
      var pos = vm.proy.empresas.indexOf(empresa);
      if(pos>-1){
        vm.proy.empresas.splice(pos, 1);
        vm.preguntarEmpresa = false;
      }
    }

    vm.onBlurPalabra = function(){
      vm.palabraAux = vm.palabraAux.trim();
      if(vm.palabraAux!="" && vm.proy.palabras.indexOf(vm.palabraAux)==-1){
        vm.proy.palabras.push(vm.palabraAux);
      }
      vm.palabraAux = "";
    }

    vm.removeColumna = function(col){
      $http.delete("/api/KanbanColumn",{params: {colId: col._id, proyId:vm.proy._id}})
      .success(function(response){
        //console.log(response);
        //console.log("Columna eliminada correctamente");
        vm.preguntarColumna = false;
        vm.actualizado = true;
        $interval(function () {
          vm.actualizado = false;
        }, 2000, 1);
      })
      .error(function(errData){
        console.log("ERROR:"+errData.error);
        console.log(errData.err);
      });
    }

    vm.cambioPalabra = function(event){//funcion para crear lista de responsables, que se llamará cada vez que se tipee una coma
      //console.log(event);
      if(event.key==","){//presiono una coma
        vm.palabraAux = vm.palabraAux.replace(/,/g,"");
        vm.palabraAux = vm.palabraAux.trim();
        if(vm.palabraAux!="" && vm.proy.palabras.indexOf(vm.palabraAux)==-1){
          vm.proy.palabras.push(vm.palabraAux);
        }
        vm.palabraAux = "";
      }
    }

    vm.removePalabra = function(palabra){
      var pos = vm.proy.palabras.indexOf(palabra);
      if(pos>-1){
        vm.proy.palabras.splice(pos, 1);
      }
    }

    var cargarThumbnail = function(usuario){
      $http.get("/api/userImage/thumbnail", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.thumbnail = response.ruta;
      });
    };
    var cargarFoto = function(usuario){
      var now = new Date().getTime();
      $http.get("/api/userImage", {params:{userId:usuario._id}})
      .success(function(response){
        usuario.image = response.ruta+"?ts="+now;
      });
    };


    var buscarParticipante = function(user, lista){
      //console.log("Buscando "+user._source.name+" en ");
      //console.log(lista);
      for (var i = 0; i < lista.length; i++) {
        if(lista[i]._id == user._id){
          return i;
        }
      }
      return -1;
    };

    vm.subirFotoProy = function(){
      var file = vm.foto;
      //console.log('file is:');
      //console.dir(file);

      var fd = new FormData();//necesario para hacer un post
      fd.append('file', file);
      fd.append('proyId',vm.proy._id);

      $http.post("api/foto/proyecto", fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
      .success(function(){
         $window.location.reload();
      })
      .error(function(errData){
        console.log("Error:"+errData.error);
        console.log(errData.err);
      });
    }

  };
})();
