(function() {

  angular
    .module('meanApp')
    .controller('projectNewCtrl', projectNewCtrl);

  projectNewCtrl.$inject = ['$http','$interval','$location', 'authentication','$routeParams','$window'];
  function projectNewCtrl($http,$interval,$location, authentication,$routeParams,$window) {
    var vm = this;
    if(!authentication.isLoggedIn()){
      $location.path('/')
    }
    var user = authentication.currentUser();
    var proyName = $routeParams['proyName'];

    //console.log("En proyectoNewCtrl proyName es "+proyName);


    vm.cambio = function(event){//funcion para crear lista de responsables, que se llamará cada vez que se tipee una coma
      //console.log(event);
      if(event.key==","){//presiono una coma
        vm.aux = vm.aux.replace(/,/g,"");
        vm.aux = vm.aux.trim();
        if(vm.aux!="")
          vm.datos.palabras.push(vm.aux);
        vm.aux = "";
      }
    }
    vm.remove = function(name){
      var pos = vm.datos.palabras.indexOf(name);
      if(pos>-1){
        vm.datos.palabras.splice(pos, 1);
      }
    }
    vm.aux = "";
    vm.onBlur = function(){
      vm.aux = vm.aux.trim();
      if(vm.aux!=""){
        vm.datos.palabras.push(vm.aux);
        vm.aux = "";
      }
    }

    if($window.localStorage.getItem('mm-nuevoProyecto')===null){
      vm.datos = {nombre:proyName ,corta:"",larga:"", facilitador:user._id, palabras:[]};
    }
    else{
      vm.datos = JSON.parse($window.localStorage['mm-nuevoProyecto']);
    }

    vm.saved = false;
    vm.onSubmit = function(){
      //console.log('Enviando datos de Proyecto '+ vm.datos.email + " - " + vm.datos.nombre+ " - " +vm.datos.corta + " - "+ vm.datos.larga);
      //console.log("name: "+user.name+" email: "+user.email);
      //console.log("Facilitador : "+vm.datos.facilitador);
      vm.aux = vm.aux.trim();
      if(vm.aux!=""){
        vm.datos.palabras.push(vm.aux);
        vm.aux = "";
      }

      $window.localStorage['mm-nuevoProyecto'] = JSON.stringify(vm.datos);
      $location.path('/new/project/2/');
    };


    vm.volver = function(){
      window.history.back();
    }
  };

})();
