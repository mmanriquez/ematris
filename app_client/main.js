(function () {

  angular.module('meanApp', ['ngRoute','dndLists']);



  function config ($routeProvider, $locationProvider) {
    //usar rutas en ingles para el facilitador, y en español para el emprendedor
    //en la ruta usar primero la accion y luego el objeto sobre el cual se realiza la accion
    //por ejemplo "list/project" es una página que lista los proyectos del facilitador
    $routeProvider
      .when('/', {
        templateUrl: 'home/home.view.html',
        controller: 'homeCtrl',
        controllerAs: 'vm'
      })
      .when('/activar/:invitadoId',{
        templateUrl: '/auth/register/activar.view.html',
        controller: 'activarCtrl',
        controllerAs: 'vm'
      })
      .when('/register', {
        templateUrl: '/auth/register/register.view.html',
        controller: 'registerCtrl',
        controllerAs: 'vm'
      })
      .when('/login', {
        templateUrl: '/auth/login/login.view.html',
        controller: 'loginCtrl',
        controllerAs: 'vm'
      })
      .when('/profile/:userId?', {
        templateUrl: '/profile/profile.view.html',
        controller: 'profileCtrl',
        controllerAs: 'vm'
      })
      .when('/proyecto/:userId', {//vista de un proyecto desde el punto de vista de un emprendedor
        templateUrl: '/proyecto/proyecto.view.html',
        controller: 'proyectoCtrl',
        controllerAs: 'vm'
      })
      .when('/nuevo/proyecto', {//nuevo proyecto desde el punto de vista del emprendedor
        templateUrl: '/proyecto/proyectoNuevo.view.html',
        controller: 'proyectoNuevoCtrl',
        controllerAs: 'vm'
      })
      .when('/new/project/',{//nuevo proyecto desde el punto de vista del facilitador
        templateUrl: '/proyecto/newProject.view.html',
        controller: 'projectNewCtrl',
        controllerAs: 'vm'
      })
      .when('/new/project/2/',{
        templateUrl: '/proyecto/newProject2.view.html',
        controller: 'projectNew2Ctrl',
        controllerAs: 'vm'
      })
      .when('/new/project/3/',{
        templateUrl: '/proyecto/newProject3.view.html',
        controller: 'projectNew3Ctrl',
        controllerAs: 'vm'
      })
      .when('/list/project',{
        templateUrl: '/proyecto/proyectoList.view.html',
        controller: 'proyectoListCtrl',
        controllerAs: 'vm'
      })
      .when('/kanban/:proyId',{
        templateUrl: '/proyecto/facilitador/kanban.view.html',
        controller: 'kanbanCtrl',
        controllerAs: 'vm'
      })
      //.when('/new/task/:colid/:proyId/:taskLength',{
      .when('/new/task/:colid/:proyId',{
        templateUrl: '/proyecto/newTask.view.html',
        controller: 'newTaskCtrl',
        controllerAs: 'vm'
      })
      .when('/ruta/:proyId',{
        templateUrl: '/proyecto/facilitador/ruta.view.html',
        controller: 'rutaCtrl',
        controllerAs: 'vm'
      })
      .when('/task/:proyId/:taskId',{
        templateUrl:'/proyecto/facilitador/task.view.html',
        controller:'taskCtrl',
        controllerAs: 'vm'
      })
      .when('/new/hito/:proyId/:numHitos',{
        templateUrl: '/proyecto/facilitador/newHito.view.html',
        controller: 'hitoNewCtrl',
        controllerAs: 'vm'
      })
      .when('/new/reunion/:proyId',{
        templateUrl: '/proyecto/facilitador/newReunion.view.html',
        controller: 'reunionNewCtrl',
        controllerAs: 'vm'
      })
      .when('/reuniones/:proyId',{
        templateUrl: '/proyecto/facilitador/reuniones.view.html',
        controller: 'reunionesCtrl',
        controllerAs: 'vm'
      })
      .when('/muro/:proyId',{
        templateUrl: '/proyecto/facilitador/muro.view.html',
        controller: 'muroCtrl',
        controllerAs: 'vm'
      })
      .when('/new/discusion/:proyId',{
        templateUrl: '/proyecto/facilitador/newDiscusion.view.html',
        controller: 'newDiscusionCtrl',
        controllerAs: 'vm'
      })
      .when('/edit/task/:proyId/:taskId',{
        templateUrl: '/proyecto/editTask.view.html',
        controller: 'editTaskCtrl',
        controllerAs: 'vm'
      })
      .when('/participants/:proyId',{
        templateUrl: '/proyecto/facilitador/participants.view.html',
        controller: 'participantsCtrl',
        controllerAs: 'vm'
      })
      .when('/edit',{
        templateUrl: '/edit/edit.view.html',
        controller: 'editCtrl',
        controllerAs: 'vm'
      })
      .when('/discusion/:proyId/:discusionId',{
        templateUrl: '/proyecto/discusion.view.html',
        controller: 'discusionCtrl',
        controllerAs: 'vm'
      })
      .when('/documentos/:proyId',{
        templateUrl: '/proyecto/listaDocumentos.view.html',
        controller: 'listDocCtrl',
        controllerAs: 'vm'
      })
      .when('/configuracion/:proyId',{
        templateUrl: '/proyecto/configuracion.view.html',
        controller: 'configCtrl',
        controllerAs: 'vm'
      })
      .when('/new/empresa/:proyId',{
        templateUrl: '/proyecto/newEmpresa.view.html',
        controller: 'newEmpresaCtrl',
        controllerAs: 'vm'
      })
      .when('/empresa/:proyId/:empId',{
        templateUrl: '/proyecto/empresa.view.html',
        controller: 'empresaCtrl',
        controllerAs: 'vm'
      })
      /*.when('/project/:proyId',{//vista de un proyecto desde el punto de vista del facilitador
        templateUrl: '/proyecto/facilitador/project.view.html',
        controller: 'projectFacilitadorCtrl',
        controllerAs: 'vm'
      })*/
      .otherwise({redirectTo: '/'});

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
  }

  function run($rootScope, $location, authentication) {
    /*var paginas = ['/profile/:userId?','/proyecto/:userId','/nuevo/proyecto','/new/project/','/new/project/2/','/new/project/3/','/list/project','/kanban/:proyId','/new/task/:colid/:proyId',
    '/ruta/:proyId','/task/:proyId/:taskId','/new/hito/:proyId/:numHitos','/new/reunion/:proyId','/reuniones/:proyId','/muro/:proyId','/new/discusion/:proyId','/edit/task/:proyId/:taskId',
    '/participants/:proyId','/edit','/discusion/:proyId/:discusionId','/documentos/:proyId','/configuracion/:proyId','/new/empresa/:proyId','/empresa/:proyId/:empId'];*/
    /*$rootScope.$on('$routeChangeStart', function(event, nextRoute, currentRoute) {
      if ($location.path() === '/profile' && !authentication.isLoggedIn()) {
        $location.path('/');
      }
    });*/
  }

  angular
    .module('meanApp')
    .config(['$routeProvider', '$locationProvider', config])
    .run(['$rootScope', '$location', 'authentication', run]);

})();
