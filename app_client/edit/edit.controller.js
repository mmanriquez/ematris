(function() {

  angular
    .module('meanApp')
    .controller('editCtrl', editCtrl);

  editCtrl.$inject = ['$location', 'authentication', 'meanData'];
  function editCtrl($location, authentication, meanData) {
    var vm = this;
    vm.user = {};

    var token  = $location.search().token;

      if(token){
      	 authentication.saveToken(token);


	    meanData.getProfile()
	      .success(function(data) {
	        vm.user = data;
	      })
	      .error(function (e) {
	        console.log(e);
	      });
      }

    vm.onSubmit = function () {
      console.log("user: "+JSON.stringify(vm.user, null, 4));
      authentication
        .edit(vm.user)
        .error(function(err){
          alert(err);
        })
        .then(function(){
          $location.path('profile');
        });
    };

    vm.cambio = function(event){//funcion para crear lista de skills, que se llamará cada vez que se tipee una coma
        //console.log(event);
        if(event.key==","){//presiono una coma
          vm.aux = vm.aux.replace(",","");
          vm.user.skills.indexOf(vm.aux) === -1 ? vm.user.skills.push(vm.aux) : console.log("This item already exists");
          //vm.skills.push(vm.aux);
          vm.aux = "";
        }
      };

    vm.remove = function(name){
        var pos = vm.user.skills.indexOf(name);
        if(pos>-1){
          vm.user.skills.splice(pos, 1);
        }
      };

    vm.aux = "";
      vm.onBlur = function(){
        vm.aux = vm.aux.trim();
        if(vm.aux!=""){
          vm.user.skills.indexOf(vm.aux) === -1 ? vm.user.skills.push(vm.aux) : console.log("This item already exists");
          vm.aux = "";
        }
      }

  }

})();
