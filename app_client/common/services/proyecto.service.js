(function() {
  angular
    .module('meanApp')
    .service('projectService', projectService);

    projectService.$inject = ['$http', '$window','$q'];

    function projectService($http, $window,$q){

      var getProyecto = function(proyId){
        return $q(function(resolve, reject){
          $http({url: "api/proyectoFromId", method: "GET",params: {proyId:proyId}})
          .success(function(data){
            resolve(data);
          })
          .error(function(errData){
            reject(errData);
          });
        });
      };

      var getProyImage = function(proyId){
        return $q(function(resolve, reject){
          $http.get("api/foto/proyecto",{params: {proyId:proyId}})
          .success(function(data){
              resolve(data);
          })
          .error(function(errData){
            reject(errData);
          });
        });
      }

      /*//ya no guardo los datos del proyecto en el navegador ya que necesito que me muestre valores actualizados de la cantidad de hitos que tiene el proyecto
      var getProyecto = function(proyId){
        return $q(function(resolve, reject){
          if(proyId == readProyectoId() && proyId != undefined){
            console.log("El proyecto estaba guardado ("+proyId+")");
            //return readProyecto();
            resolve(readProyecto());
          }
          else{
            console.log("No estaba guardado ("+proyId+")");
            $http({url: "api/proyectoFromId", method: "GET",params: {proyId:proyId}})
                  .success(function(data){
                    saveProyecto(data);
                    resolve(readProyecto());
                  })
                  .error(function(errData){
                    sinProyecto();
                    reject(errData);
                  });
          }
        });
      };*/

      var readProyectoId = function(){
        return $window.localStorage['mm-proyid'];
      };

      var saveProyectoId = function(id){
        $window.localStorage['mm-proyid'] = id;
        $window.localStorage['mm-hayProyecto'] = true;
      };

      var saveProyecto = function(proyecto){
        saveProyectoId(proyecto._id);
        $window.localStorage['mm-proy'] = JSON.stringify(proyecto);
        console.log("GUARDADO: "+$window.localStorage['mm-proy']);
      }

      var readProyecto = function(){
        var valor = $window.localStorage['mm-proy'];
        //console.log("LEIDO: "+valor && JSON.parse(valor));
        return valor && JSON.parse(valor);
      }

      var sinProyecto = function(){
        $window.localStorage['mm-hayProyecto'] = false;
      };

      var hayProyecto = function(){
        return $window.localStorage['mm-hayProyecto'];
      };

      return { getProyecto: getProyecto,
            readProyectoId: readProyectoId,
            readProyecto: readProyecto,
            saveProyectoId: saveProyectoId,
            saveProyecto: saveProyecto,
          sinProyecto: sinProyecto,
          hayProyecto: hayProyecto};
    };

})();
