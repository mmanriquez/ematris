(function () {

  angular
    .module('meanApp')
    .service('authentication', authentication);

  authentication.$inject = ['$http', '$window'];
  function authentication ($http, $window) {

    var getThumbnail = function(){
      user = currentUser();
      return $http.get("/api/userImage/thumbnail", {params:{userId:user._id}});
    };

    var getUserImame = function(){
      user = currentUser();
      return $http.get("/api/userImage", {params:{userId:user._id}});
    }

    var saveToken = function (token) {
      $window.localStorage['mean-token'] = token;
    };

    var getToken = function () {
      return $window.localStorage['mean-token'];
    };

    var isLoggedIn = function() {
      var token = getToken();
      var payload;

      if(token){
        payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var currentUser = function() {
      if(isLoggedIn()){
        var token = getToken();
        var payload = token.split('.')[1];
        payload = $window.atob(payload);
        payload = JSON.parse(payload);
        //payload.name = payload.name.toLowerCase();
        payload.name = payload.name.replace("Ã¡","á");
        payload.name = payload.name.replace("Ã©","é");
        payload.name = payload.name.replace("Ã­","í");
        payload.name = payload.name.replace("Ã³","ó");
        payload.name = payload.name.replace("Ãº","ú");
        payload.name = payload.name.replace("Ã±","ñ");
        payload.name = payload.name.replace("Ã¼","ü");

        /*var aux = payload.name.split(" ");
        aux.forEach(function(palabra, index){
          aux[index]= palabra.charAt(0).toUpperCase() + palabra.slice(1);
        });*/

        return {
          _id: payload._id,
          email : payload.email,
          name : payload.name,
          rol : payload.rol
        };
      }
    };

    var mayuscula =

    register = function(user) {
      return $http.post('/api/register', user).success(function(data){
        saveToken(data.token);
      });
    };

    activar = function(user){
      return $http.post('/api/activar', user).success(function(data){
        saveToken(data.token);
      });
    }

    login = function(user) {
      return $http.post('/api/login', user).success(function(data) {
        saveToken(data.token);
      });
    };

    linkedinLogout = function(){
      var linken = $window.open("https://www.linkedin.com/m/logout/");
      setTimeout(function () { linken.close();}, 3000);
    };

    logout = function() {
      $window.localStorage.removeItem('mean-token');
    };

    edit = function(user) {
      return $http.post('/api/edit', user).success(function(data){
        saveToken(data.token);
      });
    };

    return {
      getUserImame : getUserImame,
      getThumbnail :getThumbnail,
      currentUser : currentUser,
      saveToken : saveToken,
      getToken : getToken,
      isLoggedIn : isLoggedIn,
      register : register,
      activar : activar,
      login : login,
      logout : logout,
      linkedinLogout: linkedinLogout,
      edit: edit
    };
  }


})();
