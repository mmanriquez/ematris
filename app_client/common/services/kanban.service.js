(function(){

  angular
    .module('meanApp')
    .service('kanbanService', kanbanService);

  kanbanService.$inject = ['$http'];

  function kanbanService($http){
    var getColumns = function (proyId) {
        $http({url: "api/KanbanBoard", method: "GET",params: proyId})
              .success(function(data){
                return data;
              })
              .error(function(errData){
                return null;
              });
    };

    var createColumn = function(data){
      $http.post('/api/KanbanColumn', data).then(function success(response){
          return response;
      }, function error(response){
        console.log("ERROR:"+response.error);
      });
    }

    return {
      //canMoveTask: canMoveTask,
      //moveTask: moveTask,
      //createTask: createTask,
      //modifyTask: modifyTask,
      //deleteTask: deleteTask,
      //createColumn: createColumn,
      //deleteColumn: deleteColumn,
      //modifyColumn: modifyColumn,
        getColumns: getColumns
    };
  }
})();


/*
router.get('/KanbanBoard',ctrlKanban.getColumnasTareas);
router.post('/KanbanColumn',ctrlKanban.insertColumna);
router.post('/KanbanTask',ctrlKanban.insertTarea);
router.put('/setKanbanColumn',ctrlKanban.setColumna);
router.put('/setKanbanTask',ctrlKanban.setTarea);

router.delete('/KanbanColumn',ctrlKanban.delColumna);
router.delete('/KanbanTask',ctrlKanban.delTarea);
*/
