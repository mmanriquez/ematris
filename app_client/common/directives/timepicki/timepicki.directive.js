(function () {

  angular
    .module('meanApp')
    .directive('jqtimepicki', jqtimepicki);

  function jqtimepicki() {
    return {
      restrict: 'A',
      require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
          element.timepicki({
        		show_meridian:false,
        		min_hour_value:0,
        		max_hour_value:23,
        		step_size_minutes:15,
        		overflow_minutes:true,
        		increase_direction:'up',
            start_time: ["08", "00"],
        		disable_keyboard_mobile: true,
            on_change:function(){
              //console.log(element[0].value);
              ctrl.$setViewValue(element[0].value);
              scope.$apply();
            }
          });
         }
    }
  }


})();
