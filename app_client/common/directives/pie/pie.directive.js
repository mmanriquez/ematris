(function () {

  angular
    .module('meanApp')
    .directive('pie', pie);

  function pie () {
    return {
      restrict: 'EA',
      templateUrl: '/common/directives/pie/pie.template.html'
    };
  }

})();
