(function () {

  angular
    .module('meanApp')
    .directive('sideBar', sideBar);
  sideBar.$inject = ['$location'];
  function sideBar ($location) {
    var link = function(scope,element, attrs){
      
      scope.vm.path = $location.path().split('/')[1];
    }
    return {
      restrict: 'E',
      templateUrl: '/common/directives/side-bar/sidebar.template.html',
      link:link
    };
  }

})();
