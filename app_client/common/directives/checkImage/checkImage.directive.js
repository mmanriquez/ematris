(function () {

  angular
    .module('meanApp')
    .directive('checkImage', checkImage);


  checkImage.$inject = ['$http'];
  function checkImage ($http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            attrs.$observe('ngSrc', function(ngSrc) {
                $http.get(ngSrc).success(function(){
                    console.log("Existe la imagen!");
                }).error(function(){
                    alert('image not exist');
                    element.attr('src', '/profiles/male.png'); // set default image
                });
            });
        }
    };
  }

})();
