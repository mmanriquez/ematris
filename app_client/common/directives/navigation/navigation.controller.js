(function () {

  angular
    .module('meanApp')
    .controller('navigationCtrl', navigationCtrl);

  navigationCtrl.$inject = ['$location','authentication'];
  function navigationCtrl($location, authentication) {
    var vm = this;
    //pongo unos acentos para forzar a que guarde en utf8 áéíóú
    vm.isLoggedIn = authentication.isLoggedIn();

    vm.currentUser = authentication.currentUser();
    /*console.log(vm.currentUser);
    vm.currentUser.name  = vm.currentUser.name.replace("á","&aacute;");
    vm.currentUser.name = vm.currentUser.name.replace("é","&eacute;");
    vm.currentUser.name = vm.currentUser.name.replace("í","&iacute;");
    vm.currentUser.name = vm.currentUser.name.replace("ó","&oacute;");
    vm.currentUser.name = vm.currentUser.name.replace("ú","&uacute;");
    vm.currentUser.name = vm.currentUser.name.replace("ñ","&ntilde;");*/

    if(vm.currentUser!=undefined){
      if(vm.currentUser.rol=="emprendedor"){
        vm.isEmprendedor = true;
        vm.isMentor = false;
        vm.isFacilitador = false;
        //vm.bg = "bgEmprendedor"
      }
      if(vm.currentUser.rol=="mentor"){
        vm.isEmprendedor = false;
        vm.isMentor = true;
        vm.isFacilitador = false;
        //vm.bg = "bgMentor"
      }
      if(vm.currentUser.rol=="facilitador"){
        vm.isEmprendedor = false;
        vm.isMentor = false;
        vm.isFacilitador = true;
        //vm.bg = "bgFacilitador"
      }
    }

    vm.logout = function (){
      //borrar datos guardados en el navegador!!
        authentication.logout();
    }

    if (vm.isLoggedIn){
      authentication.getUserImame()
      .success(function(response){
        now = new Date().getTime();
        vm.currentUser.image = response.ruta+"?ts="+now;
      }).error(function(errData){
        vm.currentUser.image = "/images/user/ico-user-fem-celeste.svg";
      });
    }

  }

})();
