(function () {

  angular
    .module('meanApp')
    .directive('jqdatepicker', jqdatepicker);

  function jqdatepicker() {
    return {
      restrict: 'A',
      require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            element.datepicker({
              closeText: 'Cerrar',
              prevText: '< Ant',
              nextText: 'Sig >',
              currentText: 'Hoy',
              monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
              monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
              dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
              dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
              dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
              weekHeader: 'Sm',
              dateFormat: 'dd/mm/yy',
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: false,
              yearSuffix: '',
                onSelect: function (date) {
                        ctrl.$setViewValue(date);
                        scope.$apply();
                }
            });
          }
    }
  }

})();
