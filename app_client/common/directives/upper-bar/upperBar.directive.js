(function () {

  angular
    .module('meanApp')
    .directive('upperBar', upperBar);

  function upperBar () {
    var link = function(scope,element, attrs){
      scope.vm.pageTitle = attrs.name;
    }
    return {
      restrict: 'E',
      templateUrl: '/common/directives/upper-bar/upperbar.template.html',
      link:link
    };
  }

})();
