(function () {

  angular
    .module('meanApp')
    .controller('projectCtrl', projectCtrl);

  projectCtrl.$inject = ['projectService'];
  function projectCtrl(projectService) {
    var projvm = this;
    //projvm.proy.nombre="";

    if(projectService.hayProyecto()){
        var id = projectService.readProyectoId();
        promesa = projectService.getProyecto(id);
        promesa.then(function(proyecto){
          projvm.proy = proyecto;
        },function(error){
          projvm.proy = null;
        });
    }

  }

})();
