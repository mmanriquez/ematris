(function () {

  angular
    .module('meanApp')
    .directive('project', project);

  function project () {
    //console.log("DESDE DIRECTIVA:"+mmProyid);
    return {
      restrict: 'EA',
      scope:{mmProyid:'@'},
      templateUrl: '/common/directives/project/project.template.html',
      controller: 'projectCtrl as projvm'
    };
  }

})();
