(function () {

  angular
  .module('meanApp')
  .controller('loginCtrl', loginCtrl);

  loginCtrl.$inject = ['$location', 'authentication','$interval'];
  function loginCtrl($location, authentication,$interval) {
    var vm = this;

    vm.credentials = {
      email : "",
      password : ""
    };


    vm.mostrar = false;
    vm.onSubmit = function () {
      authentication
        .login(vm.credentials)
        .error(function(err){
          console.log(err);
          vm.mensajeError = err.error;
          vm.mostrar = true;
          $interval(function () {
            vm.mostrar = false;
          }, 2000, 1);
        })
        .then(function(){
          $location.path('list/project');
        });
    };
  }
})();
