(function () {

  angular
  .module('meanApp')
  .controller('activarCtrl', activarCtrl);

  activarCtrl.$inject = ['$routeParams', '$http','$interval','$location','authentication'];
  function activarCtrl($routeParams, $http,$interval,$location,authentication) {
    var vm = this;

    vm.user = {};
    vm.pass = "";
    vm.pass2 = "";

    $http.get('/api/public/profile',{params:{userId:$routeParams.invitadoId}})
    .success(function(response){
      console.log(response);
      if(response.rol!="invitado"){
        $location.path("/register");
      }
      else{
        vm.user = response;
      }
    })
    .error(function(errData){
      console.log(errData);
      $location.path("/register");
    });


    vm.activar = function(){
      if(vm.pass!=vm.pass2){
        return;
      }
      var user = vm.user;
      user.skills = vm.skills;
      user.redesSociales = vm.redesSociales;
      user.password = vm.pass;
      console.log("Activando ");
      console.log(user);
      authentication.activar(user)
      .success(function(){
        $location.path('/list/project');
      })
      .error(function(errData){

      });
    }

    vm.aux = "";
    vm.cambio = function(event){//funcion para crear lista de skills, que se llamará cada vez que se tipee una coma
      //console.log(event);
      if(event.key==","){//presiono una coma
        vm.aux = vm.aux.replace(",","");
        vm.skills.indexOf(vm.aux) === -1 ? vm.skills.push(vm.aux) : console.log("This item already exists");
        //vm.skills.push(vm.aux);
        vm.aux = "";
      }
    };

    vm.auxSocial = "";
    vm.cambioSocial = function(event){//funcion para crear lista de skills, que se llamará cada vez que se tipee una coma
        //console.log(event);
        if(event.key==","){//presiono una coma
          vm.auxSocial = vm.auxSocial.replace(",","");
          vm.redesSociales.indexOf(vm.auxSocial) === -1 ? vm.redesSociales.push(vm.auxSocial) : console.log("This item already exists");
          //vm.skills.push(vm.aux);
          vm.auxSocial = "";
        }
      };

    vm.remove = function(name){
      var pos = vm.skills.indexOf(name);
      if(pos>-1){
        vm.skills.splice(pos, 1);
      }
    };
    vm.removeSocials = function(red){
      var pos = vm.redesSociales.indexOf(red);
      if(pos>-1){
        vm.redesSociales.splice(pos, 1);
      }
    }


    vm.skills = [];
    vm.onBlur = function(){
      vm.aux = vm.aux.trim();
      if(vm.aux!=""){
        vm.skills.indexOf(vm.aux) === -1 ? vm.skills.push(vm.aux) : console.log("This item already exists");
        vm.aux = "";
      }
    }


    vm.redesSociales = [];
    vm.onBlurSocial = function(){
      vm.auxSocial = vm.auxSocial.trim();
      if(vm.auxSocial!=""){
        vm.redesSociales.indexOf(vm.auxSocial) === -1 ? vm.redesSociales.push(vm.auxSocial) : console.log("This item already exists");
        vm.auxSocial = "";
      }
    }
  }
})();
