(function () {

  angular
    .module('meanApp')
    .controller('registerCtrl', registerCtrl);

  registerCtrl.$inject = ['$location', 'authentication'];
  function registerCtrl($location, authentication) {
    var vm = this;

    vm.credentials = {
      name : "",
      email : "",
      password : "",
      rol: "emprendedor",
      redesSociales: [],
      skills: []
    };

    vm.onSubmit = function () {
      console.log('Submitting registration');
      console.log("Rol: "+vm.credentials.rol);
      authentication
        .register(vm.credentials)
        .error(function(err){
          alert(err);
        })
        .then(function(){
          $location.path('profile');
        });
    };

    vm.cambio = function(event){//funcion para crear lista de skills, que se llamará cada vez que se tipee una coma
      //console.log(event);
      if(event.key==","){//presiono una coma
        vm.aux = vm.aux.replace(",","");
        vm.credentials.skills.indexOf(vm.aux) === -1 ? vm.credentials.skills.push(vm.aux) : console.log("This item already exists");
        //vm.skills.push(vm.aux);
        vm.aux = "";
      }
    };

    vm.cambioSocial = function(event){//funcion para crear lista de skills, que se llamará cada vez que se tipee una coma
        //console.log(event);
        if(event.key==","){//presiono una coma
          vm.auxSocial = vm.auxSocial.replace(",","");
          vm.credentials.redesSociales.indexOf(vm.auxSocial) === -1 ? vm.credentials.redesSociales.push(vm.auxSocial) : console.log("This item already exists");
          //vm.skills.push(vm.aux);
          vm.auxSocial = "";
        }
      };

    vm.remove = function(name){
      var pos = vm.credentials.skills.indexOf(name);
      if(pos>-1){
        vm.credentials.skills.splice(pos, 1);
      }
    };
    vm.removeSocials = function(red){
      var pos = vm.credentials.redesSociales.indexOf(red);
      if(pos>-1){
        vm.credentials.redesSociales.splice(pos, 1);
      }
    }

    vm.aux = "";
    vm.onBlur = function(){
      vm.aux = vm.aux.trim();
      if(vm.aux!=""){
        vm.credentials.skills.indexOf(vm.aux) === -1 ? vm.credentials.skills.push(vm.aux) : console.log("This item already exists");
        vm.aux = "";
      }
    }

    vm.auxSocial = "";
    vm.onBlurSocial = function(){
      vm.auxSocial = vm.auxSocial.trim();
      if(vm.auxSocial!=""){
        vm.credentials.redesSociales.indexOf(vm.auxSocial) === -1 ? vm.credentials.redesSociales.push(vm.auxSocial) : console.log("This item already exists");
        vm.auxSocial = "";
      }
    }

  }

})();
