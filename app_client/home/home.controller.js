(function() {

  angular
    .module('meanApp')
    .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$location', 'authentication'];

    function homeCtrl ($location, authentication) {
      var vm = this;

      //console.log('Home controller is running');
      var token  = $location.search().token;

      if(token){
      	 authentication.saveToken(token);
      	 //console.log(authentication.isLoggedIn());
      }

      vm.isLoggedIn = function () {
        return authentication.isLoggedIn();

      };

      vm.logoutLinken = function () {
        authentication
          .linkedinLogout()
          .error(function(err){
            alert(err.message);
          })
          .then(function(){
            $location.path('/');
          });
      };

    }

})();
