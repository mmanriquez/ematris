var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: process.env.KEY_EMATRIS,
  userProperty: 'payload'
});



var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');
var ctrlProy = require('../controllers/proyecto');
var ctrlFacilitadores = require('../controllers/facilitadores');
var ctrlEmprendedores = require('../controllers/emprendedores');
var ctrlKanban = require('../controllers/kanban');
var ctrlHitos = require('../controllers/hitos');
var ctrlTareas = require('../controllers/tareas');
var ctrlReuniones = require('../controllers/reunion');
var ctrlDiscusiones = require('../controllers/discusion');
var ctrlUtil = require('../controllers/util');




router.post('/invitar',ctrlUtil.sendMail);

// profile
router.get('/profile', auth, ctrlProfile.profileRead);
router.get('/public/profile',ctrlProfile.getProfile);
router.post('/edit', ctrlProfile.rolEdit);
router.post('/foto',ctrlProfile.subirFoto)
router.get('/userImage',ctrlProfile.getUserImage);
router.get('/userImage/thumbnail',ctrlProfile.getUserImageThumb);

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.post('/activar',ctrlAuth.activar);


//empresas
router.post('/empresa', ctrlProy.addEmpresa);
router.get('/empresa',ctrlProy.getEmpresaFromId);
router.post('/empresa/editar',ctrlProy.editarEmpresa);


//proyecto
router.post('/proyecto',ctrlProy.crearProyecto);
//router.post('/proyecto/editar',ctrlProy.editarProyecto);
router.post('/proyecto/actualizar', ctrlProy.actualizarProyecto);
router.get('/proyecto',ctrlProy.getProyecto);
router.get('/proyectoFromId',ctrlProy.getProyectoFromId);
router.get('/proyecto/list',ctrlProy.getListProyectos);
router.get('/proyecto/getRecomendados', ctrlProy.getRecomendados);
router.get('/proyecto/queryMentores',ctrlProy.queryMentores);
router.get('/proyecto/queryParticipantes',ctrlProy.queryParticipantes);
router.get('/proyecto/getParticipantes', ctrlProy.getParticipantes);
router.get('/facilitadores',ctrlFacilitadores.getFacilitadores);
router.get('/documentos',ctrlProy.listaDocumentos);
router.get('/tareas/documentos',ctrlProy.tareasConDoc);
router.post('/foto/proyecto', ctrlProy.subirFoto);
//router.get('/foto/proyecto',ctrlProy.getProyImage);


//lista emprendedores
router.get('/emprendedores',ctrlEmprendedores.getEmprendedores);
router.get('/auth/linkedin', ctrlAuth.linkedin);
router.get('/auth/linkedin/callback', ctrlAuth.linkedinCall);

//datos de una columna
router.get('/columna',ctrlKanban.getColumna);
//datos de una tarea
router.get('/tarea',ctrlKanban.getTarea);
//recuperar los datos de la ultima tarea para mostrar en lista de proyectos
router.get('/ultimaTarea',ctrlKanban.getLastTarea)
//ingresar un nuevo comentario a una TAREA dada
router.post('/comentario',ctrlKanban.insertComentario)

//comentarios de una tarea
//esta demas ya que al recuperar una tarea en especifico tambien recupero los comentarios
router.get('/comentarios',ctrlTareas.getComentarios);
//recupera las columnas y sus tareas de un proyecto
router.get('/KanbanBoard',ctrlKanban.getColumnasTareas);
//recupera datos de las columnas, pero no de las tareas
router.get('/columnas',ctrlKanban.getColumnas);
//recupera las tareas que no estan asignadas a ningun hito
router.get('/tareasSinHito',ctrlHitos.getTareasSinHito);
//recupero los hitos de un proyecto
router.get('/hitos', ctrlHitos.getHitos);
//ingreso un nuevo hito
router.post('/hito', ctrlHitos.nuevoHito);
//ingreso una nueva columna
router.post('/KanbanColumn',ctrlKanban.insertColumna);
//ingreso una nueva tarea (en una columna específica)
router.post('/KanbanTask',ctrlKanban.insertTarea);
//TODO: crear un metodo post que cree una tarea en un hito específico (y colocar el link en el detalle de un hito)
//TODO: crear un metodo post que cree una tarea sola, en una columna por defecto (POR HACER) (y colocar el link en el muro del proyecto)


//ingreso una nueva reunion
router.post('/reunion',ctrlReuniones.nuevaReunion);
//recupera las reuniones de un proyecto
router.get('/reuniones',ctrlReuniones.getReuniones);


//actualizar
router.put('/setKanbanColumn',ctrlKanban.setColumna);
router.post('/setKanbanTask',ctrlKanban.setTarea);

//actualiza las columnas origen y destino, ademas del orden de cada una de las tareas involucradas en el hecho de mover una tarea de una columna a otra
router.put('/setKanbanTasks',ctrlKanban.setTareas);
router.post('/proyecto/mentor', ctrlProy.nuevoRecomendado);

router.delete('/KanbanColumn',ctrlKanban.delColumna);
router.delete('/KanbanTask',ctrlKanban.delTareaHito);

router.get('/discusiones',ctrlDiscusiones.getDiscusiones);
router.post('/discusion',ctrlDiscusiones.nuevaDiscusion);
router.get('/discusion',ctrlDiscusiones.getDiscusion);
router.post('/discusion/comentario',ctrlDiscusiones.insertComentario);
router.post('/discusion/takeaway', ctrlDiscusiones.insertTakeaway);
router.get('/discusion/takeaway', ctrlDiscusiones.getTakeaways);
router.delete('/discusion/takeaway',ctrlDiscusiones.removeTakeaway);
router.post('/like/comentario',ctrlDiscusiones.commentAddLike);
router.post('/unlike/comentario',ctrlDiscusiones.commentRemoveLike);
router.post('/like/discussion',ctrlDiscusiones.discussionAddLike);
router.post('/unlike/discussion',ctrlDiscusiones.discussionRemoveLike);


console.log("en index.js");

module.exports = router;
