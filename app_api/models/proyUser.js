var mongoose = require('mongoose')
var relationship = require("mongoose-relationship");
var Schema = mongoose.Schema;

var proyUserSchema =  new mongoose.Schema({
  user : {type: Schema.Types.ObjectId, ref: 'User'},
  proyecto : {type: Schema.Types.ObjectId, ref: 'Proyecto'}
});

mongoose.model('ProyUser', proyUserSchema);
