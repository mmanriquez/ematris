var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;
var mongoosastic = require('mongoosastic');

var proyectoSchema = new mongoose.Schema({
  nombre : {type: String, required:true},
  descripcionCorta : {type:String},
  descripcionCompleta : {type:String},
  fecha : { type:Date, required:true},
  columnas: [{type: Schema.Types.ObjectId, ref: 'Columna'}],
  colDefecto: {type: Schema.Types.ObjectId, ref: 'Columna'},
  hitos:[{type: Schema.Types.ObjectId, ref:'Hito'}],
  destacados:[{type: Schema.Types.ObjectId, ref:'Comentario'}],
  discusiones:[{type: Schema.Types.ObjectId, ref: 'Discusion'}],
  palabras:[{type: String}],
  participantes:[{type: Schema.Types.ObjectId, ref: 'User'}],
  invitados:[{type: String}],
  empresas:[{type: Schema.Types.ObjectId, ref: 'Empresa'}]
},{
    timestamps: true
});

proyectoSchema.plugin(mongoosastic);

mongoose.model('Proyecto', proyectoSchema);

var empresaSchema = new mongoose.Schema({
  nombre: {type: String, required: true},
  urlvideo:{type:String},
  quehace:{type:String},
  industria:{type:String},
  estado:{type:String},
  oportunidad:{type:String},
  competencia:{type:String},
  solucion:{type:String},
  clientes:{type:String},
  modelo:{type:String},
  impacto:{type:String},
  participantes: [{rol:String, user: {type: Schema.Types.ObjectId, ref: 'User'}}],//solo emprendedores??
  proyectos: [{type: Schema.Types.ObjectId, ref: 'Proyecto'}]
});

mongoose.model('Empresa', empresaSchema);
