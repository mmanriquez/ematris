var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;


var discusionSchema = new mongoose.Schema({
  titulo : {type: String, required:true},
  discusion : {type: String},
  fecha : {type:Date},
  autor : {type: Schema.Types.ObjectId, ref: 'User'},
  likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
  comentarios: [{type: Schema.Types.ObjectId, ref: 'Comentario'}]
});

mongoose.model('Discusion', discusionSchema);
