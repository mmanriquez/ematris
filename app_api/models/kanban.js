var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var docSchema = new mongoose.Schema({
  nombre : {type:String},
  fecha : {type:Date},
  pie : {type:String},
  proyecto : {type: Schema.Types.ObjectId, ref: 'Proyecto'}
});

mongoose.model('Documento', docSchema);

var tareaSchema = new mongoose.Schema({
  nombre : { type: String, required: true },
  descripcion : {type: String, required: true },
  fechaCreacion:{type: Date },
  fechaini: {type: Date },
  fechafin: {type: Date },
  orden: {type: Number},
  responsables: [{type: Schema.Types.ObjectId, ref: 'User'}],
  etiqueta : {type: Schema.Types.ObjectId, ref: 'Etiqueta'},
  creador: {type: Schema.Types.ObjectId, ref: 'User'},
  actualizador : {type: Schema.Types.ObjectId, ref: 'User'},
  documentos : [{type: Schema.Types.ObjectId, ref: 'Documento'}],
  //columna : {type: Schema.Types.ObjectId, ref: 'Columna'},
  comentarios:[{type: Schema.Types.ObjectId, ref: 'Comentario'}]
  //comentarios: [comentariosSchema]
},{
    timestamps: true
});
mongoose.model('Tarea', tareaSchema);

var columnaSchema = new mongoose.Schema({
  nombre : { type: String, required: true },
  orden : {type: Number, min: 0},
  wip : {type: Number,  min: 0},
  tareas: [{type: Schema.Types.ObjectId, ref: 'Tarea'}]
  //tareas: [tareaSchema]
  //proyecto : {type: Schema.Types.ObjectId, ref: 'Proyecto'}
});
mongoose.model('Columna', columnaSchema);

var etiquetaSchema = new mongoose.Schema({
  nombre :{ type: String, unique: true, required: true },
  color : { type: String}
});

mongoose.model('Etiqueta', etiquetaSchema);
