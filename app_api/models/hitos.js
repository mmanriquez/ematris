var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var hitoSchema = new mongoose.Schema({
  nombre : {type: String, required:true},
  orden: {type: Number},
  tareas: [{type: Schema.Types.ObjectId, ref: 'Tarea'}]
});



mongoose.model('Hito', hitoSchema);
