var mongoose = require('mongoose')
var relationship = require("mongoose-relationship");
var Schema = mongoose.Schema;

var proyMentorSchema =  new mongoose.Schema({
  mentor : {type: Schema.Types.ObjectId, ref: 'User'},
  proyecto : {type: Schema.Types.ObjectId, ref: 'Proyecto'}
});

mongoose.model('ProyMentor', proyMentorSchema);
