var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;


var comentariosSchema = new mongoose.Schema({
  texto : { type: String, required: true },
  fecha : {type: Date, required: true },
  hallazgo : {type:Boolean},
  tarea: {type: Boolean},
  likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
  autor : {type: Schema.Types.ObjectId, ref: 'User'}
});
mongoose.model('Comentario', comentariosSchema);
