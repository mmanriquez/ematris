var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoosastic = require('mongoosastic');

var Schema = mongoose.Schema;


var userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    es_indexed:true
  },
  name: {
    type: String,
    required: true,
    es_indexed:true
  },
  hash: String,
  salt: String,
  notificacionDiscusion : {type:Boolean,default: true},
  notificacionComentarioTarea : {type:Boolean,default: true},
  notificacionKanban : {type:Boolean,default: true},
  redesSociales: [{type:String, es_indexed:true}],
  rol: { type: String, default: 'emprendedor', es_indexed:true  },
  accessToken: String,
  refreshToken: String,
  idLinkedin: String,
  headline: {type:String, es_indexed:true},
  industry: {type:String, es_indexed:true},
  summary: {type:String, es_indexed:true},
  specialties: {type:String, es_indexed:true},
  skills: {type:[String], es_indexed:true},  //las habilidades no se pueden obtener sin partner
  proyectos:[{type: Schema.Types.ObjectId, ref: 'Proyecto'}]
});

userSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
};

userSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);
  //console.log("Generando JWT: NOMBRE:"+this.name);

  var sign = jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    rol: this.rol,
    exp: parseInt(expiry.getTime() / 1000),
  }, process.env.KEY_EMATRIS);// DO NOT KEEP YOUR SECRET IN THE CODE!
  console.log(sign);
  return sign;
};

userSchema.plugin(mongoosastic);


var User = mongoose.model('User', userSchema);
User.createMapping({
  "settings": {
    "analysis": {
      "filter": {
        "spanish_stop": {
          "type":       "stop",
          "stopwords":  "_spanish_"
        },
        "spanish_keywords": {
          "type":       "keyword_marker",
          "keywords":   [""]
        },
        "spanish_stemmer": {
          "type":       "stemmer",
          "language":   "light_spanish"
        }
      },
      "analyzer": {
        "spanish": {
          "tokenizer":  "standard",
          "filter": [
            "lowercase",
            "spanish_stop",
            "spanish_keywords",
            "spanish_stemmer"
          ]
        }
      }
    }
  },
  "mappings": {
    "user": {
      "_all": {
        "analyzer": "spanish",
        "search_analyzer": "spanish"
      }
    }
  }
});
var count = 0;
var stream = User.synchronize();

stream.on('data', function(err, doc){
  count++;
});
stream.on('close', function(){
  console.log('Se indexaron ' + count + ' documentos!');
});
stream.on('error', function(err){
  console.log(err);
});
