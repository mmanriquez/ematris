var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var reunionSchema = new mongoose.Schema({
  nombre : {type: String, required:true},
  creador: {type: Schema.Types.ObjectId, ref: 'User'},
  objetivos:{type: String},
  fecha: {type: Date},
  horaini :{type: Date},
  horafin :{type: Date},
  lugar: {type:String},
  participantes: [{type: String}],//lista de usuarios registrados
  minuta: {type:String}
});

mongoose.model('Reunion', reunionSchema);
