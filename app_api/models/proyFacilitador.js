var mongoose = require('mongoose')
var relationship = require("mongoose-relationship");
var Schema = mongoose.Schema;

var proyFacilitadorSchema =  new mongoose.Schema({
  facilitador : {type: Schema.Types.ObjectId, ref: 'User'},
  proyecto : {type: Schema.Types.ObjectId, ref: 'Proyecto'}
});

mongoose.model('ProyFacilitador', proyFacilitadorSchema);
