var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var LinkedinStrategy = require('passport-linkedin-oauth2').Strategy;

passport.use(new LocalStrategy({
    usernameField: 'email'
  },
  function(username, password, done) {
    User.findOne({ email: username }, function (err, user) {
      if (err) { return done(err); }
      // Return if user not found in database
      if (!user) {
        return done(null, false, {
          message: 'User not found'
        });
      }
      // Return if password is wrong
      if (!user.validPassword(password)) {
        return done(null, false, {
          message: 'Password is wrong'
        });
      }
      // If credentials are correct, return the user object
      return done(null, user);
    });
  }
));

var LINKEDIN_CLIENT_ID = "78o71w1kwf2u4o";
var LINKEDIN_CLIENT_SECRET = "WFFo9ZFw4JbURJYP";

/*passport.use(new LinkedInStrategy({
    clientID: LINKEDIN_API_KEY,
    clientSecret: LINKEDIN_SECRET_KEY,
    callbackURL: "http://127.0.0.1:3000/api/auth/linkedin/callback",
    scope: ['r_emailaddress', 'r_basicprofile'],
    state: true
  },
  function(accessToken, refreshToken, profile, done) {
  // asynchronous verification, for effect... 
  process.nextTick(function () {
    // To keep the example simple, the user's LinkedIn profile is returned to 
    // represent the logged-in user. In a typical application, you would want 
    // to associate the LinkedIn account with a user record in your database, 
    // and return that user instead. 
    return done(null, profile);
  });
  }
));*/


// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete Linkedin profile is
//   serialized and deserialized.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});


passport.use(new LinkedinStrategy({
    clientID:     LINKEDIN_CLIENT_ID,
    clientSecret: LINKEDIN_CLIENT_SECRET,
    callbackURL:  "http://localhost:3000/api/auth/linkedin/callback",
    scope:        [ 'r_basicprofile', 'r_emailaddress'],
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    //req.session.accessToken = accessToken;

    var providerData = profile._json; //aca llegan los datos de linkedin
   

    //var token = user.schema.methods.generateJwt();
    //req.session.accessToken = token;
    process.nextTick(function () {
      // To keep the example simple, the user's Linkedin profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the Linkedin account with a user record in your database,
      // and return that user instead.
      var userFind = User.findOne({ email: providerData.emailAddress }, function (err, user) {
        if (err) { return done(err); }
        // Return if user not found in database
        if (!user) {
          var user = new User();

          user.accessToken = accessToken;
          user.refreshToken = refreshToken;
          
          user.name = providerData.formattedName;
          user.email = providerData.emailAddress;
          user.idLinkedin = providerData.id;
          user.headline = providerData.headline;
          user.industry = providerData.industry;
          user.summary = providerData.summary;
          user.specialties = providerData.specialties;
          user.rol = "mentor";
          user.__v = 0;
          //user.positions.title = providerData.positions.tittle;
         // user.positions.summary = providerData.positions.summary;
          user.save();
          return done(null, user);
        }

     
      return done(null, user);
      });
      
    });
  }
));