var nodemailer = require('nodemailer');

module.exports.sendMail = function(req, res){
  console.log(req.body);
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'incubando.bot@gmail.com',
      pass: 'CLAVEbotincubando'
    }
  });

  var mailOptions = {
    from: 'incubando.bot@gmail.com',
    to: 'manuel.manriquez.lopez@gmail.com',
    subject: 'invitando a '+req.body.correo,
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  };

  transporter.sendMail(mailOptions, function(err, info){
    if (err) {
      console.log(err);
      res.status(500).json({error:"Al enviar el correo de invitacion",err:err});
    } else {
      console.log('Email sent: ' + info.response);
      res.status(200).send(req.body.correo);
    }
  });
};
