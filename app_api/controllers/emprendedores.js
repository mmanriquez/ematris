var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports.getEmprendedores = function(req, res) {
  User.find({rol:"emprendedor"})
      .exec(function(err, user) {
        res.status(200).json(user);
      });
};
