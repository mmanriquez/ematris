var mongoose = require('mongoose');
var Tarea = mongoose.model('Tarea');
var Hito = mongoose.model('Comentario');
var ObjectId = mongoose.Types.ObjectId;

module.exports.getComentarios = function(req, res){
    console.log(req.query);
    if(req.query.taskId){
      Tarea.findOne({_id:new ObjectId(req.query.taskId)})
      .populate('comentarios')
      .exec(function(err, tareaEncontrada){
        if(err){
          res.status(500).json({error: "No existe la tarea"});
        }
        else{
          res.status(200).json(tareaEncontrada);
        }
      });
    }
    else{
      res.status(500).json({error:"No esta definido un id de tarea"});
    }
};
