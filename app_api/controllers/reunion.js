var mongoose = require('mongoose');
var Proyecto = mongoose.model('Proyecto');
var Reunion = mongoose.model('Reunion');


module.exports.getReuniones = function(req, res){
    if(req.query.proyId){
      Proyecto.findOne({_id:req.query.proyId})
      .populate('reuniones')
      .exec(function(err, proyectoEncontrado){
        if(err){
          res.status(500).json({error: "No existe el proyecto"});
        }
        else{
          console.log(proyectoEncontrado);
          res.status(200).json(proyectoEncontrado);
        }
      });
    }
    else{
      res.status(500).json({error:"No esta definido un id de proyecto"});
    }
};

module.exports.nuevaReunion = function(req, res){
  if(req.body.proyId){
    Proyecto.findOne({_id:req.body.proyId})
    .exec(function(err, proyectoEncontrado){
      if(err){
        res.status(500).json({error:"Al seleccionar el proyecto",err:err});
      }
      else{
        var reunion = new Reunion();
        reunion.nombre = req.body.nombre;
        reunion.creador = req.body.creador;
        reunion.objetivos = req.body.objetivos;
        var aux = req.body.fecha.split("/");
        reunion.fecha = new Date(aux[2],aux[1]-1,aux[0],0,0,0,0);
        var auxini = req.body.horaini.split(":");
        reunion.horaini = new Date(aux[2],aux[1]-1,aux[0],auxini[0],auxini[1],0,0);
        var auxfin = req.body.horafin.split(":");
        reunion.horafin = new Date(aux[2],aux[1]-1,aux[0],auxfin[0],auxfin[1],0,0);
        reunion.lugar = req.body.lugar;
        reunion.participantes = req.body.participantes;

        reunion.save(function(err2, reunionGuardada){
          if(err2){
            res.status(500).json({error:"Al guardar la reunion",err:err2});
          }
          else{
            proyectoEncontrado.reuniones.push(reunionGuardada._id);
            proyectoEncontrado.save(function(err3, proyectoActualizado){
                if(err3){
                  res.status(500).json({error:"Al actualizar el proyecto",err:err3});
                }
                else{
                  res.status(200).json(reunionGuardada);
                }
            });
          }
        });
      }

    });

  }

};
