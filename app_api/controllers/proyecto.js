var mongoose = require('mongoose');
var glob = require('glob');
var path = require('path');
var fs = require('fs');
//para enviar correos
var nodemailer = require('nodemailer');

var Columna = mongoose.model('Columna');
var Proyecto = mongoose.model('Proyecto');
var ProyUser = mongoose.model('ProyUser');
var ProyFacilitador = mongoose.model('ProyFacilitador');
var ProyMentor = mongoose.model('ProyMentor');
var User = mongoose.model('User');
var Documento = mongoose.model('Documento');
var Tarea = mongoose.model('Tarea');
var Empresa = mongoose.model('Empresa');
var ObjectId = mongoose.Types.ObjectId;




module.exports.subirFoto = function(req, res){
  if (!req.files){
      return res.status(400).send('No se envió una foto.');
  }
  var base = __dirname.split("/");
  base.pop();//quito controllers
  base.pop();//quito app_api
  var ruta = base.join("/")+"/public/proyectos/";

  var proyId = req.body.proyId;
  //busco si existe una imagen anterior de este usuario para eliminarla
  //console.log("Buscando: "+ruta+userId+".*");
  glob(ruta+proyId+".*", function(err, files){
    if(!err && files.length>0){//existe al menos una imagen anterior, por lo tanto debo eliminarla
      for (var i = 0; i < files.length; i++) {
        console.log("Eliminando: "+files[i]);
        //remuevo las posibles imagenes
        fs.unlink(files[i],function(err2){
          if(err2){
            console.log("Error: al eliminar la imagen.");
            console.log(err2);
          }
        });
      }
    }

    var sampleFile = req.files.file;//file viene de la llave usada en formData.append('file',valor)


    //console.log(sampleFile);
    var extension = path.extname(sampleFile.name);
    //console.log(extension);
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(ruta+proyId+extension, function(err) {
      if (err){
        return res.status(500).json({error:"Al mover la imagen a la ruta predeterminada.",err:err});
      }
      else{
        res.status(200).send("OK");
      }
    });
  });
};


module.exports.addEmpresa = function(req, res){
  Proyecto.findOne({_id: new ObjectId(req.body.proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto asociado", err:err});
    }
    else{
      var empresa = new Empresa();
      empresa.nombre = req.body.nombre;
      empresa.quehace = req.body.quehace;
      empresa.urlvideo = req.body.urlvideo;
      empresa.industria = req.body.industria;
      empresa.estado = req.body.estado;
      empresa.oportunidad = req.body.oportunidad;
      empresa.competencia = req.body.competencia;
      empresa.solucion = req.body.solucion;
      empresa.clientes = req.body.clientes;
      empresa.modelo = req.body.modelo;
      empresa.impacto = req.body.impacto;
      empresa.participantes = req.body.participantes;
      //empresa.proyectos = [];
      empresa.proyectos.push(req.body.proyId);

      empresa.save(function(err2, empresaGuardada){
        if(err2){
          res.status(500).json({error: "Al guardar la empresa", err:err2});
        }
        else{
          //Agregar id de la empresaGuardada al proyectoEncontrado
          if(!proyectoEncontrado.empresas){
            proyectoEncontrado.empresas = [];
          }
          proyectoEncontrado.empresas.push(empresaGuardada._id);
          proyectoEncontrado.save(function(err3, proyectoActualizado){
            if(err3){
              res.status(500).json({error: "Al actualizar el proyecto que relacionado con la empresa", err:err3});
            }
            else{
              res.status(200).json(empresaGuardada);
            }
          });
        }
      });
    }
  });
};

module.exports.crearProyecto = function(req, res){
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'incubando.bot@gmail.com',
      pass: 'CLAVEbotincubando'
    }
  });
  var enviarCorreo = function(invitado){
    var pie = "Entre <a href='http://138.68.56.114/activar/"+invitado._id+"'>aquí</a> para activar su cuenta.";
    var mailOptions = {
      from: 'incubando.bot@gmail.com',
      to: invitado.correo,
      subject: 'invitando a '+invitado.correo + ' - '+invitado.name,
      text: invitado.invitacion
    };

    transporter.sendMail(mailOptions, function(err, info){
      if (err) {
        console.log(err);
        //res.status(500).json({error:"Al enviar el correo de invitacion",err:err});
      } else {
        console.log('Email sent: ' + info.response);
        //res.status(200).send(req.body.correo);
      }
    });
  };

  var nuevoProyecto = function(arrayColumnasIds){
    var nuevo = new Proyecto();

    crearUsuarios(nuevo._id, function(invitados){
      nuevo.nombre = req.body.nombre;
      nuevo.descripcionCorta = req.body.corta;
      nuevo.descripcionCompleta = req.body.larga;
      nuevo.fecha = new Date();
      nuevo.columnas = arrayColumnasIds;
      nuevo.colDefecto = arrayColumnasIds[0];//pongo por defecto la columna POR HACER
      nuevo.participantes = invitados.concat(req.body.participantes.concat(req.body.mentores.concat(req.body.facilitador)));
      nuevo.palabras = req.body.palabras;

      nuevo.save(function(err, proyGuardado){
        if(err){
          console.log("Fallo al guardar proyecto.");
          console.log(err);
          res.status(500).json({error:"Fallo al guardar proyecto.", err:err});
          return;
        }
        else{
          User.find({_id:{$in:proyGuardado.participantes}})
          .exec(function(err2, usuarios){
            if(err2){
              console.log("Fallo al buscar participantes");
              res.status(500).json({error:"Al buscar participantes", err:err2});
            }
            else{
              var largoUsers = usuarios.length;
              var encontroError = false;
              var errorEncontrado = null;
              usuarios.forEach(function(user, index){
                if(user.proyectos){
                  user.proyectos.push(proyGuardado._id);
                }
                else{
                  user.proyectos = [];
                  user.proyectos.push(proyGuardado._id);
                }
                user.save(function(err3, usuarioActualizado){
                  if(err3){
                    encontroError = true;
                    errorEncontrado = err3;
                  }
                  if(!--largoUsers){
                    if(encontroError){
                      console.log("ERROR: Al actualizar la lista de proyectos del usuario");
                      res.status(500).json({error:"Al actualizar la lista de proyectos asociado al usuario",err:errorEncontrado});
                    }
                    else{
                      res.status(200).json(proyGuardado);
                    }
                  }
                });
              });
            }
          });
        }
      });
    });
    /*
    nombre : {type: String, required:true},
    descripcionCorta : {type:String},
    descripcionCompleta : {type:String},
    fecha : { type:Date, required:true},
    columnas: [{type: Schema.Types.ObjectId, ref: 'Columna'}],
    colDefecto: {type: Schema.Types.ObjectId, ref: 'Columna'},
    hitos:[{type: Schema.Types.ObjectId, ref:'Hito'}],
    destacados:[{type: Schema.Types.ObjectId, ref:'Comentario'}],
    discusiones:[{type: Schema.Types.ObjectId, ref: 'Discusion'}]
    */
  };

  var crearColumnas = function(){
    var defecto = ["POR HACER","HACIENDO","HECHO"];
    var arrayColumnasIds = [];
    var arraycols = [];
    var cant = defecto.length;
    var huboError = false;
    var error = null;
    for (var i = 0; i < defecto.length; i++) {
      var columna = new Columna();
      columna.nombre = defecto[i];
      columna.orden = i;
      columna.wip = i==0?0:3;
      arrayColumnasIds.push(columna._id);
      arraycols.push(columna);
      arraycols[i].save(function(err, colCreada){
        if(err){
          huboError = true;
          error = err;
        }
        if(!--cant){
          if(!huboError){
            nuevoProyecto(arrayColumnasIds);
          }
          else{
            console.log("ERROR: Al crear una columna");
            res.status(500).json({error:"Al crear una columna", err:error});
          }
        }
      });
    }
  };


  var crearUsuarios = function(proyId, callback){
    var cant = req.body.invitados;
    var idsInvitados = [];
    if(cant>0){
      req.body.invitados.forEach(function(item, index){
        console.log(item);
        var invitado = new User();
        invitado.email = item.correo;
        invitado.name = item.name;
        invitado.rol = "invitado";
        invitado.proyectos = [proyId];
        invitado.save(function(err, usuarioCreado){
          if(err){
            console.log("ERROR: Al crear un invitado.");
            console.log(err);
          }
          else{
            idsInvitados.push(usuarioCreado._id);
            enviarCorreo(usuarioCreado);
          }
          //TODO: Probar si la weaita funciona xDDD
          if(!--cant){
            //Alguna funcion que siga en la logica de creacion del proyecto
            console.log(idsInvitados);
            callback(idsInvitados);
          }
        });

      });
    }
    else{
      callback([]);
    }
  }

  crearColumnas();

};

module.exports.listaDocumentos = function(req, res){
  Documento.find({proyecto:req.query.proyId})
  .exec(function(err, docsEncontrados){
    if(err){
      res.status(500).json({error:"Al buscar documentos de un proyecto", err:err});
    }
    else{
      res.status(200).json(docsEncontrados);
    }
  });
};

module.exports.tareasConDoc = function(req, res){
  if(!req.query.proyId){//debo mover esa verificacion al lado del cliente!!!!
    res.status(500).json({
      "message" : "ERROR: No hay un proyecto seleccionado",
      missing : true
    });
  }
  else{
     console.log("En getColumnas")
     Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
     .exec(function(err, proyRecuperado){
       if(err || proyRecuperado==null){
         console.log("ERROR: Al recuperar el proyecto que tiene las columnas");
         res.status(500).json({error:"Al recuperar el proyecto que tiene las columnas.", err:err});
       }
       else{
         Columna.find({"_id":{"$in":proyRecuperado.columnas}})
         .exec(function(err2,columnas){
           if(err2){
             console.log("ERROR: Al recuperar las columnas asociadas");
             res.status(500).json({error:"Al recuperar las columnas asociadas"});
           }
           else{
             var cant = columnas.length;
             var huboError = false;
             var error = null;
             columnas.forEach(function(col, index){
               Tarea.find({_id:{$in:col.tareas}, documentos:{ $gt: [] }})
               .populate("creador documentos")
               .exec(function(errTarea, tareasRecuperadas){
                 if(errTarea){
                   huboError = true;
                   error = errTarea;
                 }
                 else{
                   columnas[index].tareas = tareasRecuperadas;
                 }
                 if(!--cant){
                   if(huboError){
                     res.status(500).json({error:"Al recuperar las tareas de cada columna", err:error});
                   }
                   else{
                     console.log("columnas recuperadas");
                     res.status(200).json(columnas);
                   }
                 }
               });

             });
           }

         });

       }
     });
  }

};

module.exports.actualizarProyecto = function(req, res){
  Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto actualizado", err:err});
    }
    else{
      var nuevos = req.body.datos;



      var participantesNuevos = nuevos.participantes.filter(a=>!proyectoEncontrado.participantes.find(b=>a==b));
      var participantesSacados = proyectoEncontrado.participantes.filter(a=>!nuevos.participantes.find(b=>a==b));

      var cant = participantesNuevos.length;
      var huboError = false;
      var error = null;
      //a los nuevos participantes agregar el proyectoEncontrado entre sus proyectos
      for (var i = 0; i < participantesNuevos.length; i++) {
        User.findById(participantesNuevos[i])
        .exec(function(err2,user){
          if(err2){
            huboError = true;
            error = err2;
          }
          else{
            var pos = user.proyectos.indexOf(proyectoEncontrado._id);
            //lo agrego solo si no estaba...
            if(pos==-1){
              user.proyectos.push(proyectoEncontrado._id);
              user.save();//a rogar que todo salga bien xD
            }
          }
          if(!--cant){
            if(huboError){
              res.status(500).json({error:"Al actualizar informacion de los nuevos participantes del proyecto",error:error});
            }
            else {
              console.log("Todos los nuevos usuarios actualizados ok");
            }
          }
        });
      }


      // a los participantes que se sacaron del proyecto, quitarle el proyecto
      cant = participantesSacados.length;
      huboError = false;
      error = null;
      for (var i = 0; i < participantesSacados.length; i++) {
        User.findById(participantesSacados[i])
        .exec(function(err2,user){
          if(err2){
            huboError = true;
            error = err2;
          }
          else{
            var pos = user.proyectos.indexOf(proyectoEncontrado._id);
            if(pos>=0){
              user.proyectos.splice(pos, 1);
              user.save();//a rogar que todo salga bien xD
            }
          }
          if(!--cant){
            if(huboError){
              res.status(500).json({error:"Al actualizar informacion de los nuevos participantes del proyecto",error:error});
            }
            else {
              console.log("Todos los antiguos usuarios actualizados ok");
            }
          }
        });
      }



      proyectoEncontrado.nombre = nuevos.nombre;
      proyectoEncontrado.descripcionCorta = nuevos.descripcionCorta;
      proyectoEncontrado.descripcionCompleta = nuevos.descripcionCompleta;
      proyectoEncontrado.palabras = nuevos.palabras;
      proyectoEncontrado.participantes = nuevos.participantes;

      var eliminar = proyectoEncontrado.empresas;
      var pos = -1;
      for (var i = 0; i < nuevos.empresas.length; i++) {
        pos = eliminar.indexOf(nuevos.empresas[i]);
        if(pos >= 0){
          eliminar.splice(pos,1);
        }
      }
      proyectoEncontrado.empresas = nuevos.empresas;
      proyectoEncontrado.save(function(err2, proyectoActualizado){
        if(err2){
          res.status(500).json({error:"Al actualizar el proyecto en la base de datos", err:err2});
        }
        else{
          Empresa.remove({_id:{$in:eliminar}},function(err3){
            if(err3){
              res.status(500).json({error:"Al remover una empresa",err:err3});
            }
            else{
              res.status(200).send("OK");
            }
          });
        }
      });
    }
  });
}

module.exports.editarEmpresa = function(req, res){
  console.log("Buscando la empresa "+req.body._id);
  Empresa.findOne({_id: new ObjectId(req.body._id)})
  .exec(function(err, empresaEncontrada){
    if(err || empresaEncontrada==null){
      res.status(500).json({error:"Al recuperar la empresa por id",err:err});
    }
    else{
      empresaEncontrada.urlvideo = req.body.urlvideo!="" ? req.body.urlvideo : empresaEncontrada.urlvideo;
      empresaEncontrada.quehace = req.body.quehace!="" ? req.body.quehace : empresaEncontrada.quehace;
      empresaEncontrada.industria = req.body.industria!="" ? req.body.industria : empresaEncontrada.industria;
      empresaEncontrada.estado = req.body.estado!="" ? req.body.estado : empresaEncontrada.estado;
      empresaEncontrada.oportunidad = req.body.oportunidad!="" ? req.body.oportunidad : empresaEncontrada.oportunidad;
      empresaEncontrada.competencia = req.body.competencia!="" ? req.body.competencia : empresaEncontrada.competencia;
      empresaEncontrada.solucion = req.body.solucion!="" ? req.body.solucion : empresaEncontrada.solucion;
      empresaEncontrada.clientes = req.body.clientes!="" ? req.body.clientes : empresaEncontrada.clientes;
      empresaEncontrada.modelo = req.body.modelo!="" ? req.body.modelo : empresaEncontrada.modelo;
      empresaEncontrada.impacto = req.body.impacto!="" ? req.body.impacto : empresaEncontrada.impacto;

      empresaEncontrada.save(function(err2, empresaActualizada){
        if(err2){
          res.status(500).json({error:"Al actualizar los datos de la empresa",err:err2});
        }
        else{
          res.status(200).send("OK");
        }
      });
    }
  });
};

//lee de la base de datos el proyecto asociado a un usuario en base a su id
//param: id del usuario
//retorna: proyectos

module.exports.getProyecto = function(req, res){
  User.findOne({_id:new ObjectId(req.query.userId)})
  .populate('proyectos')
  .exec(function(err, usuarioRecuperado){
    if(err){
      res.status(500).json({error:"Al recuperar el usuario", err:err});
    }
    else{
      console.log("El usuario tiene "+usuarioRecuperado.proyectos.length+" proyectos");
      res.status(200).json(usuarioRecuperado.proyectos[0]);
    }
  });
    /*console.log("Recuperando el proyecto de "+req.query.userId);
    ProyUser.findOne({user:req.query.userId})
      .exec(function(err1,relProyUser){
        if(err1 || relProyUser==null){
          console.log("ERROR: el usuario no tiene un proyecto relacionado");
          res.status(500).json({error:"El usuario no tiene un proyecto relacionado.", err:err1});
        }
        else{
          console.log("Este usuario tiene un proyecto relacionado ("+relProyUser+")");
          Proyecto.findOne({_id:relProyUser.proyecto})
            .exec(function(err2, proyRecuperado){
              if(err2){
                console.log("ERROR: al recuperar proyecto asociado");
                res.status(500).json({error:"Al recuperar proyecto asociado.", err:err2});
              }
              else{
                console.log("Su proyecto se llama "+proyRecuperado.nombre);
                res.status(201).json({proyecto: proyRecuperado});
              }
            });
        }
      });*/
};


module.exports.getListProyectos = function(req, res){
  User.findOne({_id:new ObjectId(req.query.userId)})
  .populate('proyectos')
  .exec(function(err, usuarioRecuperado){
    if(err || usuarioRecuperado==null){
      res.status(500).json({error:"Al recuperar el usuario", err:err});
    }
    else{
      console.log("El usuario tiene "+usuarioRecuperado.proyectos.length+" proyectos");
      var cant = usuarioRecuperado.proyectos.length;
      var huboError = false;
      var error = null;
      var proyectos = usuarioRecuperado.toObject().proyectos;

      if(cant==0){
        res.status(500).json({error:"El usuario no tiene proyectos",err:null});
      }
      else{
        proyectos.forEach(function(proy,index){
          glob(getProyImage(proy._id),function(err, files){
            let archivo = "/images/ico-project-"+(parseInt((index%3) + 1 ))+".svg";//foto por defecto
            if(!err && files.length>0){
              var datos = files[0].split("/");
              var largo = datos.length;
              archivo = datos[largo-2]+"/"+datos[largo-1];
            }
            proyectos[index].image = archivo;

            console.log(proyectos[index]);

            User.find({_id:{$in:proy.participantes}})
            .exec(function(err2, usuariosConsultados){
              if(err2){
                huboError = true;
                error = err2;
              }
              else{
                proyectos[index].participantes = usuariosConsultados;
              }
              if(!--cant){
                if(huboError){
                  console.log("Hubo un error");
                  res.status(500).json({error:"Al consultar usuarios relacionados",err:error});
                }
                else{
                  res.status(200).json(proyectos);
                }
              }

            })
          });
        });
      }
    }
  });
};

var getProyImage = function(proyId){
  var proy = proyId;
  var base = __dirname.split("/");
  base.pop();//quito controllers
  base.pop();//quito app_api
  var ruta = base.join("/")+"/public/proyectos/"+proy+".*";
  return ruta;
  /*glob(ruta, function(err, files){
    var archivo = "/images/ico-project-2.svg";//foto por defecto
    if(!err && files.length>0){
      var datos = files[0].split("/");
      var largo = datos.length;
      archivo = datos[largo-2]+"/"+datos[largo-1];
    }
    //res.status(200).json({ruta : archivo});
    return archivo;
  });*/
}

//lee de la base de datos los datos de un proyecto en base al id del proyecto
//param: id del proyecto
//return: datos del proyecto
module.exports.getProyectoFromId = function(req, res){
  Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
  .populate("participantes empresas")
    .exec(function(err, proyRecuperado){
      if(err || proyRecuperado==null){
        console.log("ERROR: al recuperar proyecto ("+req.query.proyId+") asociado");
        res.status(500).json({error:"Al recuperar proyecto asociado.", err:err});
      }
      else{
        resp = proyRecuperado.toObject();
        console.log("Su proyecto se llama "+resp.nombre);
        //resp.image = getProyImage(proyRecuperado._id);
        glob(getProyImage(proyRecuperado._id),function(err, files){
          var archivo = "/images/ico-project-2.svg";//foto por defecto
          if(!err && files.length>0){
            var datos = files[0].split("/");
            var largo = datos.length;
            archivo = datos[largo-2]+"/"+datos[largo-1];
          }
          resp.image = archivo;
          //console.log(resp);
          res.status(201).json(resp);
        });
        //console.log(resp);

      }
    });
}

module.exports.getEmpresaFromId = function(req, res){
  Empresa.findOne({_id: new ObjectId(req.query.empId)})
  .exec(function(err, empresaEncontrada){
    if(err){
      res.status(500).json({error:"Al buscar la empresa", err:err});
    }
    else{
      res.status(200).json(empresaEncontrada);
    }
  });

}

module.exports.queryParticipantes = function(req, res){
  User.search({
        query_string: {
          query: req.query.consulta+"*"
        }
        //"query": { "wildcard" : { "user" : req.query.consulta+"*" }  }
  }
  /*,{
    hydrate:true
  }*/,
  function (err, results) {
      if (err) {
        res.status(500).json({error:"Al consultar mentores",err:err});
        //return console.log(JSON.stringify(err, null, 4));
      }
      else{
        res.status(200).json(results.hits.hits);
      }
  });
};

module.exports.queryMentores = function(req, res){
  var terminos = req.query.palabras;
  console.log("Consultando por : ");
  console.log(terminos);
  var query = " AND rol:mentor";
  if(Array.isArray(terminos)){
    query = terminos.join(',') + query;
  }
  else{
    query = terminos + query;
  }
  console.log("QUERY: "+ query);

  //el filtrado de las consultas se definen en el modelo de usuarios
  //y al lanzar el script al inicio
  User.search({
    query_string:{
      query:query
    }
  }
  /*,{
    hydrate:true,
    hydrateWithESResults: {source: false},
    hydrateOptions: {select: '_id name email skills redesSociales'}
  }*/,
  function (err, results) {
      if (err) {
        res.status(500).json({error:"Al consultar mentores",err:err});
        //return console.log(JSON.stringify(err, null, 4));
      }
      else{
        console.log(results.hits);
        res.status(200).json(results.hits);
      }
  });
};

//lee de la base de datos los datos de un proyecto en base al id del proyecto
//param: id del proyecto
//return: mentores relacionados
module.exports.getRecomendados = function(req, res){

  Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
    .exec(function(err, proyRecuperado){
      if(err || proyRecuperado==null){
        console.log("ERROR: al recuperar proyecto ("+req.query.proyId+") asociado");
        res.status(500).json({error:"Al recuperar proyecto asociado.", err:err});
      }
      else{
        User.search({
              query_string: {
                query: "(" + proyRecuperado.descripcionCorta + " " + proyRecuperado.descripcionCompleta + ") AND rol:mentor"
              }
        },
        function (err, results) {
            if (err) {
              res.status(500).json(err);
              //return console.log(JSON.stringify(err, null, 4));
            }
            //return console.log(JSON.stringify(results.hits.hits, null, 4));
            res.status(201).json(results.hits.hits);
        });
      }
    });
}

module.exports.nuevoRecomendado = function(req, res){
  var mentores = req.body.mentores;

  console.log("Mentores: ======= " + mentores);

      Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
      .exec(function(err, proyectoRecuperado){
        if(err){
          res.status(500).json({error:"Al recuperar el proyecto para actualizar."});
        }
        else{
          try{
            for (var i = mentores.length - 1; i >= 0; i--) {
                User.findOne({_id:new ObjectId(mentores[i])})
                .exec(function(err, mentorRecuperado){
                  if(err ||  mentorRecuperado == null){
                    res.status(500).json({error:"Al recuperar el mentor para guardar."});
                  }

                  ProyMentor.findOne({mentor: mentorRecuperado, proyecto: proyectoRecuperado})
                  .exec(function(err, proyMentorRecuperado){
                    if(err ||  proyMentorRecuperado == null){
                      var proyment = new ProyMentor();
                      proyment.mentor = mentorRecuperado;
                      proyment.proyecto = proyectoRecuperado;
                      proyment.save();
                      res.status(200).json();
                    }
                    else{
                      res.status(500).json("Usuario ya se encontraba en el proyecto");
                    }
                  });
                });
            }
          }
          catch(error){
            res.status(500).json({error:error.message});
          }
        }
      });
 //   }
//  });
};


module.exports.getParticipantes = function(req, res){
  Proyecto.findOne({_id: new ObjectId(req.query.proyId)})
  .exec(function(err, proyectoRecuperado){
    if(err || proyectoRecuperado==null){
      res.status(500).json({error:"Al recuperar el proyecto para actualizar."});
    }
    else{
      console.log("ObjectId(\""+proyectoRecuperado._id+"\")");
        ProyMentor.find({proyecto: proyectoRecuperado._id})
        .exec(function(err, proyectoMentorRecuperado){
          if(err || proyectoMentorRecuperado==null){
            res.status(500).json({error:"Al recuperar el ProyMentor para actualizar."});
          }
          else{
          var ids = [];
          for (var variable in proyectoMentorRecuperado) {
            ids.push(proyectoMentorRecuperado[variable].mentor);
          }
            User.find({_id: {$in: ids}})
            .exec(function(err, participantes){
              if(err || participantes==null){
                res.status(500).json({error:"Al recuperar participantes para actualizar."});
              }
              else{
                res.status(201).json(participantes);
              }
            });
          }
        });
    }
  });
}
