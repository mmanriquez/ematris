var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};




module.exports.register = function(req, res) {

  if(!req.body.name || !req.body.email || !req.body.password) {
    sendJSONresponse(res, 400, {
     "message": "Se requieren los campos nombre, email y password"
    });
    return;
  }

  var user = new User();

  user.name = req.body.name;
  user.email = req.body.email;
  user.rol = req.body.rol;
  user.skills = req.body.skills;
  user.proyectos = [];
  user.redesSociales = req.body.redesSociales;

  user.setPassword(req.body.password);

  user.save(function(err) {
    var token;
    token = user.generateJwt();
    res.status(200);
    res.json({
      "token" : token
    });
  });

};


//activar en el fondo solo cambia el password y asigna habilidades
//ademas de redes sociales
module.exports.activar = function(req, res){
  User.findById(req.body._id)
  .exec(function(err, userEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el usuario a activar",err:err});
    }
    else{
      userEncontrado.skills = req.body.skills;
      userEncontrado.redesSociales = req.body.redesSociales;
      userEncontrado.rol = "mentor";
      userEncontrado.setPassword(req.body.password);

      userEncontrado.save(function(err2){
        var token;
        token = userEncontrado.generateJwt();
        res.status(200);
        res.json({
          "token" : token
        });
      });
    }
  });
};

module.exports.changePassword = function(req, res){
  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    User.findById(req.payload._id)
    .exec(function(err, user) {
      if(err){
        res.status(500).json({error:"Al buscar el usuario",err:err});
      }
      else{
        if(user.validPassword(req.body.oldPassword)){
          user.setPassword(req.body.newPassword);
          user.save(function(err, userActualizado){
            if(err){
              res.status(500).json({error:"Al actualizar el password",err:err});
            }
            else{
              var token;
              token = userActualizado.generateJwt();
              res.status(200);
              res.json({
                "token" : token
              });
            }
          });
        }
      }
    });
  }

};

module.exports.login = function(req, res) {

  // if(!req.body.email || !req.body.password) {
  //   sendJSONresponse(res, 400, {
  //     "message": "All fields required"
  //   });
  //   return;
  // }

  passport.authenticate('local', function(err, user, info){
    var token;

    // If Passport throws/catches an error
    if (err) {
      res.status(404).json({error:"Al autenticar.",err:err});
      return;
    }

    // If a user is found
    if(user){
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token" : token
      });
    }
    else {
      // If user is not found
      if(info.message.indexOf("Password")>=0){
        res.status(401).json({error:"Password incorrecto",err:info});
      }
      else if(info.message.indexOf("User")>=0){
        res.status(401).json({error:"No se encontró el usuario",err:info});
      }
      else{
        res.status(401).json({error:"Error desconocido. Intente de nuevo",err:info});
      }
    }
  }) (req, res);

};

module.exports.linkedin = function(req, res) {
  passport.authenticate('linkedin', { state: 'SOME STATE' },
  function(req, res){
    // The request will be redirected to Linkedin for authentication, so this
    // function will not be called.
  }) (req, res);
  //console.log("passport linkedin ok");
};


module.exports.linkedinCall = function(req, res, next) {
  passport.authenticate('linkedin', function(err, user, info) {
    if(err) {return next(err);}
    if(!user) {return res.redirect('/register');}
    if(user) {

      console.log(user);
      token = user.generateJwt();
      if (user.__v != 0) {
        return res.redirect('/' + '?token=' + token);
      }
      return res.redirect('/edit/' + '?token=' + token);
    }
  }) (req, res, next);
  console.log("callback linkedin");
};



/*
{
    successRedirect: '/',
    failureRedirect: '/login'
  }) (req, res);
  */
