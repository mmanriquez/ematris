var mongoose = require('mongoose');
var Columna = mongoose.model('Columna');
var Tarea = mongoose.model('Tarea');
var Proyecto = mongoose.model('Proyecto');
var User = mongoose.model('User');
var Hito = mongoose.model('Hito');
var ObjectId = mongoose.Types.ObjectId;

module.exports.getTareasSinHito = function(req, res){
  //unir todas las tareas que pertenecen a un hito (que pertenecen a un proyecto)
  Proyecto.findOne({_id:new ObjectId(req.query.proyId)})//primero recupero el proyecto que estamos analizando
  .populate('hitos')//pueblo el campo hitos
  .populate('columnas')
  .exec(function(err, proyectoRecuperado){
    if(err){
      res.status(500).json({error:"Al recuperar el proyecto que contiene los hitos"});
    }
    else{
      columnas = proyectoRecuperado.columnas;
      tareasProyecto = [];
      for (var i = 0; i < columnas.length; i++) {
        tareasProyecto = tareasProyecto.concat(columnas[i].tareas);
      }
      for (var i = 0; i < tareasProyecto.length; i++) {
        tareasProyecto[i] = String(tareasProyecto[i]);
      }
      console.log("Tareas del proyecto");
      console.log(tareasProyecto);
      hitos = proyectoRecuperado.hitos;
      tareasConHito = [];
      for (var i = 0; i < hitos.length; i++) {
          tareasConHito = tareasConHito.concat(hitos[i].tareas);//Hago la union de todos los id de tareas que pertenecen a un hito
      }
      for (var i = 0; i < tareasConHito.length; i++) {
        tareasConHito[i] = String(tareasConHito[i]);
      }
      console.log("Tareas en algun hito");
      console.log(tareasConHito);
      tareasSinHito = tareasProyecto.filter(function(idTarea){
        var pos = tareasConHito.indexOf(idTarea);
        console.log("Buscando "+idTarea+" en Tareas con hito. pos : "+pos);
        return pos<0;
      });

      console.log("Id de tareas sin hito");
      console.log(tareasSinHito);

      //hacer resta de todas las tareas menos la union anterior
      Tarea.find({_id:{$in:tareasSinHito}})
      .exec(function(err2, tareasRecuperadas){
        if(err2){
          res.status(500).json({error:"Al recuperar tareas sin hito"});
        }
        else{
          res.status(200).json(tareasRecuperadas);
        }
      });
    }
  });
};

module.exports.getHitos = function(req, res){
  Hito.find({_id:{$in:req.query.hitos}})
  .populate('tareas')
  .exec(function(err, hitos){
    if(err){
      res.status(500).json({error:"Al recuperar hitos."});
    }
    else{
      res.status(200).json(hitos);
    }
  });
}


module.exports.nuevoHito = function(req, res){
  var nuevoHito = new Hito();
  nuevoHito.nombre = req.body.nombre;//en un post usar body
  nuevoHito.tareas = req.body.tareas;
  nuevoHito.orden = req.body.numHitos;
  console.log(nuevoHito);
  nuevoHito.save(function(err, hitoIngresado){
    if(err){
      res.status(500).json({error:"Al ingresar nuevo hito."});
    }
    else{
      Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
      .exec(function(err, proyectoRecuperado){
        if(err){
          res.status(500).json({error:"Al recuperar el proyecto para actualizar."});
        }
        else{
          try{
            if(typeof proyectoRecuperado.hitos === 'undefined' || proyectoRecuperado.hitos== null){
              proyectoRecuperado.hitos = [];
            }
            proyectoRecuperado.hitos.push(hitoIngresado._id);
            proyectoRecuperado.save(function(err2, proyectoActualizado){
                if(err2){
                  res.status(500).json({error:"Al actualizar referencia en proyecto."});
                }
                else{
                  res.status(200).json();
                }
            });
          }
          catch(error){
            res.status(500).json({error:error.message});
          }
        }
      });
    }
  });
};
