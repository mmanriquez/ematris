var mongoose = require('mongoose');
var glob = require('glob');
var path = require('path');
var resize = require('im-resize');//dependencia: imagemagick, instalar con apt-get install imagemagick
var sizeOf = require('image-size');//hay que instalar aparte https://www.npmjs.com/package/image-size
var fs = require('fs');
var User = mongoose.model('User');

var ObjectId = mongoose.Types.ObjectId;

module.exports.getProfile = function(req, res){
  if(mongoose.Types.ObjectId.isValid(req.query.userId)){
    //console.log("El ID "+req.query.userId+" es valido");
    User.findById(req.query.userId)
    .exec(function(err, user){
      if(err){
        res.status(500).json({error:"Al buscar el usuario", err:err});
      }
      else{
        if(user!=null){
          res.status(200).json(user);
        }
        else{
          res.status(404).json({error:"No existe el usuario",err:null});
        }
      }
    });
  }
  else{
    res.status(404).json({error:"No existe el usuario",err:null});
  }
};

module.exports.profileRead = function(req, res) {

  if (!req.payload._id) {
    res.status(401).json({
      "message" : "UnauthorizedError: private profile"
    });
  } else {
    User.findById(req.payload._id)
    .exec(function(err, user) {
      res.status(200).json(user);
    });
  }

};

module.exports.rolEdit = function(req, res) {
  console.log("editando " + req.body._id + " con rol " + req.body.rol + " y skills: "+ req.body.skills);
  var query = { _id: req.body._id };
  var userEdit = User.findOneAndUpdate(query, {$set:{ rol: req.body.rol, __v: 1, skills: req.body.skills  }}, {new: true}, function(err, doc){
      if(err){
          console.log("Something wrong when updating data!");
      }

      var token;
      token = doc.generateJwt();
      res.status(200);
      res.json({
        "token" : token
      });
  });

};

module.exports.getUserImageThumb = function(req, res){
  var user = req.query.userId;
  var base = __dirname.split("/");
  base.pop();//quito controllers
  base.pop();//quito app_api
  var ruta = base.join("/")+"/public/profiles/thumbnail/"+user+".*";
  glob(ruta, function(err, files){
    var archivo = "/images/user/ico-user-fem-celeste.svg";//foto por defecto
    if(!err && files.length>0){

      var datos = files[0].split("/");
      var largo = datos.length;
      archivo = "/profiles/thumbnail/"+datos[largo-1];
      console.log("existe el thumbnail y es: "+archivo);
    }
    res.status(200).json({ruta : archivo});
  });
};

module.exports.getUserImage = function(req, res){
  var user = req.query.userId;
  var base = __dirname.split("/");
  base.pop();//quito controllers
  base.pop();//quito app_api
  var ruta = base.join("/")+"/public/profiles/"+user+".*";

  console.log("Chequeando: "+ruta);
  glob(ruta, function(err, files){
    var archivo = "/images/user/ico-user-fem-celeste.svg";//foto por defecto
    if(!err && files.length>0){
      console.log("SI EXISTE LA IMAGEN");
      var datos = files[0].split("/");
      var largo = datos.length;
      archivo = datos[largo-2]+"/"+datos[largo-1];
    }
    res.status(200).json({ruta : archivo});
  });
}

module.exports.subirFoto = function(req,res){
  if (!req.files){
      return res.status(400).send('No se envió una foto.');
  }

  var base = __dirname.split("/");
  base.pop();//quito controllers
  base.pop();//quito app_api
  var ruta = base.join("/")+"/public/profiles/";

  var userId = req.body.userId;


  //busco si existe una imagen anterior de este usuario para eliminarla
  //console.log("Buscando: "+ruta+userId+".*");
  glob(ruta+userId+".*", function(err, files){
    if(!err && files.length>0){//existe al menos una imagen anterior, por lo tanto debo eliminarla
      for (var i = 0; i < files.length; i++) {
        console.log("Eliminando: "+files[i]);
        //remuevo las posibles imagenes
        fs.unlink(files[i],function(err2){
          if(err2){
            console.log("Error: al eliminar la imagen.");
            console.log(err2);
          }
          else{
            //si existe una imagen anterior, si o si existe un thumbnail
            //asi que ahora busco los thumbnails
            glob(ruta+"thumbnail/"+userId+".*", function(err, files){
              if(!err && files.length>0){//existe al menos un thumbnail de esta imagen
                for (var i = 0; i < files.length; i++) {
                  console.log("Eliminando: "+files[i]);
                  //remuevo las posibles imagenes thumbnail
                  fs.unlink(files[i], function(err3){
                    if(err3){
                      console.log("Error: al eliminar el thumbnail de la imagen.");
                      console.log(err3);
                    }
                    //y ahora que me aseguré que elimine las posibles imagenes
                    else{
                      //guardo la nueva imagen
                      guardarImagen();
                    }
                  });
                }
              }
              //si no existe un thumbnail no elimino y guardo la imagen nueva
              else{
                guardarImagen();
              }
            });
          }
        });
      }
    }
    //si no existe una imagen anterior... lo mas probable es que tampoco exista un thumbnail asi que no pregunto por un thumbnail
    else{
      //asi que solo guardo la imagen nueva
      guardarImagen();
    }
  });

  var guardarImagen = function(){
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    var sampleFile = req.files.file;//file viene de la llave usada en formData.append('file',valor)


    //console.log(sampleFile);
    var extension = path.extname(sampleFile.name);
    //console.log(extension);
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(ruta+userId+extension, function(err) {
      if (err){
        return res.status(500).json({error:"Al mover la imagen a la ruta predeterminada.",err:err});
      }
      else{
        var dimensiones = sizeOf(ruta+userId+extension);
        var image = {
          path: ruta+userId+extension,
          width: dimensiones.width,
          height: dimensiones.height
        };

        var output = {
          path:ruta+"thumbnail/",
          versions: [{
            maxHeight: 30,
            maxWidth: 30,
            aspect: "1:1"
          }]
        };

        resize(image, output, function(err2, versions) {
          if (err2) {
            res.status(500).json({error:"Al generar el thumbnail.",err:err2});
          }
          else{
            res.status(200).send("OK");
          }
        });
      }
    });
  };
};
