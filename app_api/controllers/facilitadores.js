var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports.getFacilitadores = function(req, res) {
  User.find({rol:"facilitador"})
      .exec(function(err, user) {
        res.status(200).json(user);
      });
};
