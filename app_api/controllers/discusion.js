var mongoose = require('mongoose');
var Discusion = mongoose.model('Discusion');
var Proyecto = mongoose.model('Proyecto');
var Comentario = mongoose.model('Comentario');
var ObjectId = mongoose.Types.ObjectId;

module.exports.nuevaDiscusion = function(req, res) {
  var discusion = new Discusion();
  discusion.titulo = req.body.titulo;
  discusion.discusion = req.body.discusion;
  discusion.autor = req.body.autor;
  discusion.fecha = new Date();

  discusion.save(function(err, discusionCreada){
    if(err){
      res.status(500).json({error:"Al crear una nueva discusion",err:err});
    }
    else{
      Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
      .exec(function(err2, proyectoRecuperado){
          if(err2){
            res.status(500).json({error:"Al buscar el proyecto asociado",err:err2});
          }
          else{
            proyectoRecuperado.discusiones.push(discusionCreada._id);
            proyectoRecuperado.save(function(err3, proyectoGuardado){
              if(err3){
                res.status(500).json({error:"Al modificar el proyecto", err:err3});
              }
              else{
                res.status(200).json(discusionCreada);
              }
            });
          }

      });
    }
  });

};

module.exports.discussionRemoveLike = function(req, res){
  console.log("en discussionRemoveLike");
  console.log("Al usuario "+req.body.userId+" ya no le gusta la discusion "+req.body.discussionId);
  Discusion.findOne({_id:new ObjectId(req.body.discussionId)})
  .exec(function(err, discusionEncontrada){
    if(!discusionEncontrada.likes){
      discusionEncontrada.likes = [];
    }
    var pos = discusionEncontrada.likes.indexOf(req.body.userId);
    if(pos >= 0){
      discusionEncontrada.likes.splice(pos,1);
    }
    discusionEncontrada.save(function(err2, discusionGuardada){
      if(err2){
        res.status(500).json({error:"Al actualizar los likes del comentario", err:err2});
      }
      else{
        res.status(200).json(discusionGuardada);
      }
    });
  });
};


module.exports.commentRemoveLike = function(req, res){
  console.log("en commentRemoveLike");
  console.log("Al usuario "+req.body.userId+" ya no le gusta el comentario "+req.body.commentId);
  Comentario.findOne({_id:new ObjectId(req.body.commentId)})
  .exec(function(err, comentarioEncontrado){
    if(!comentarioEncontrado.likes){
      comentarioEncontrado.likes = [];
    }
    var pos = comentarioEncontrado.likes.indexOf(req.body.userId);
    if(pos >= 0){
      comentarioEncontrado.likes.splice(pos,1);
    }
    comentarioEncontrado.save(function(err2, comentarioGuardado){
      if(err2){
        res.status(500).json({error:"Al actualizar los likes del comentario", err:err2});
      }
      else{
        res.status(200).json(comentarioGuardado);
      }
    });
  });
};

module.exports.commentAddLike = function(req, res){
  console.log("en commentAddLike");
  console.log("Al usuario "+req.body.userId+" le gusto el comentario "+req.body.commentId);
  Comentario.findOne({_id:new ObjectId(req.body.commentId)})
  .exec(function(err, comentarioEncontrado){
    if(!comentarioEncontrado.likes){
      comentarioEncontrado.likes = [];
    }
    comentarioEncontrado.likes.push(req.body.userId);
    comentarioEncontrado.save(function(err2, comentarioGuardado){
      if(err2){
        res.status(500).json({error:"Al actualizar los likes del comentario", err:err2});
      }
      else{
        res.status(200).json(comentarioGuardado);
      }
    });
  });
};

module.exports.discussionAddLike = function(req, res){
  console.log("en discussionAddLike");
  console.log("Al usuario "+req.body.userId+" le gusto la discusion "+req.body.discussionId);
  Discusion.findOne({_id:new ObjectId(req.body.discussionId)})
  .exec(function(err, discusionEncontrada){
    if(!discusionEncontrada.likes){
      discusionEncontrada.likes = [];
    }
    discusionEncontrada.likes.push(req.body.userId);
    discusionEncontrada.save(function(err2, discusionGuardada){
      if(err2){
        res.status(500).json({error:"Al actualizar los likes de la discusion", err:err2});
      }
      else{
        res.status(200).json(discusionGuardada);
      }
    });
  });
};


module.exports.getDiscusion = function(req, res){
  Discusion.findOne({_id:new ObjectId(req.query.discusionId)})
  .populate("autor")
  .exec(function(err, discusionRecuperada){
    if(err){
      res.status(500).json({error:"Al recuperar la discusion",err:err});
    }
    else{
      Comentario.find({_id:{$in:discusionRecuperada.comentarios}})
      .populate("autor")
      .exec(function(err2, comentariosRecuperados){
        if(err2){
          res.status(500).json({error:"Al recuperar los comentarios de la discusion",err:err2});
        }
        else{
          discusionRecuperada.comentarios = comentariosRecuperados;
          res.status(200).json(discusionRecuperada);
        }
      })
    }
  });
};

module.exports.getDiscusiones = function(req, res){
  Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
  .exec(function(err, proyectoRecuperado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto asociado",err:err});
    }
    else{
      Discusion.find({_id: {$in:proyectoRecuperado.discusiones}})
      .populate('autor')
      .exec(function(err2, discusionesRecuperadas){
        if(err2){
            res.status(500).json({error:"Al buscar las discusiones asociadas", err:err2});
        }
        else{
          //res.status(200).json(discusionesRecuperadas);
          var cant = 0;
          var huboError = false;
          var errRecuperado = null;
          cant = discusionesRecuperadas.length;
          console.log("Hay "+cant+" comentarios en total");
          discusionesRecuperadas.forEach(function(item, index){
            Comentario.find({_id:{$in:item.comentarios}})
            .populate('autor')
            .exec(function(err3, comentariosRecuperados){
              if(err3){
                huboError = true;
                errRecuperado = err3;
              }
              else{
                discusionesRecuperadas[index].comentarios = comentariosRecuperados;
              }
              console.log("cant:"+cant);
              if(!--cant){
                if(huboError){
                  res.status(500).json({error:"Al popular los autores del comentario.",err:errRecuperado});
                }
                else {
                  res.status(200).json(discusionesRecuperadas);
                }
              }
            });
          });
        }
      });

    }
  });
};

module.exports.insertComentario = function(req, res){
  var nuevo = new Comentario();
  nuevo.texto = req.body.texto;
  nuevo.fecha = new Date();
  nuevo.autor = req.body.autor;

  console.log(nuevo);

  nuevo.save(function(err, comentarioGuardado){
    if(err){
      res.status(500).json({error:"Al guardar el nuevo comentario", err:err});
    }
    else{
      Discusion.findOne({_id:new ObjectId(req.body.discusionId)})
      .exec(function(err2, discusionEncontrada){
        if(err2){
          res.status(500).json({error:"Al buscar la discusion asociada", err:err2});
        }
        else{
          discusionEncontrada.comentarios.push(comentarioGuardado._id);
          discusionEncontrada.save(function(err3, discusionActualizada){
            if(err3){
              res.status(500).json({error:"Al actualizar la discusion", err:err3});
            }
            else{
              res.status(200).json(comentarioGuardado);
            }
          });
        }
      });
    }
  });
};

module.exports.removeTakeaway = function(req, res){
  console.log("En removeTakeaway");
  console.log(req.query.commentId);
  Comentario.findOne({_id:new ObjectId(req.query.commentId)})
  .exec(function(err, comentarioEncontrado){
    if(err || comentarioEncontrado==null){
      res.status(500).json({error:"Al buscar el comentario", err:err});
    }
    else{
      console.log(comentarioEncontrado);
      comentarioEncontrado.hallazgo = false;
      comentarioEncontrado.save(function(err2, comentarioActualizado){
        if(err2){
          res.status(500).json({error:"Al actualizar el comentario", err:err2});
        }
        else{
          res.status(200).send("OK");
        }
      });
    }
  });
}

module.exports.insertTakeaway = function(req, res){
  var comment = req.body.comment;
  var projectId = req.body.projectId;
  console.log(comment);
  console.log(projectId);

  Comentario.findOne({_id:new ObjectId(comment._id)})
  .exec(function(err, comment){

   if(err){
     res.status(500).json({error:"Al destacar", err:err});
   }
   comment.hallazgo = true;
   comment.save();

   Proyecto.findOne({_id:new ObjectId(projectId)})
   .exec(function(err2, proyecto){
     if(err2){
       res.status(500).json({error:"Al cargar proyecto", err:err});
     }
     proyecto.destacados.push(comment._id);
     proyecto.save(function(err3, proyectoActualizado){
       if(err3){
         res.status(500).json({error:"Al actualizar proyecto", err:err});
       }
       res.status(200).json(comment);
     })
   });
 })
}

module.exports.getTakeaways = function(req, res){
  Proyecto.findOne({_id:new ObjectId(req.query.projectId)})
  .populate('destacados')
  .exec(function(err, proyecto){
    if(err){
      res.status(500).json({error:"Al cargar proyecto", err:err});
    }
    res.status(200).json(proyecto.destacados);
  })
};
