var mongoose = require('mongoose');
var path = require('path');
var Columna = mongoose.model('Columna');
var Tarea = mongoose.model('Tarea');
var Proyecto = mongoose.model('Proyecto');
var ProyUser = mongoose.model('ProyUser');
var ProyFacilitador = mongoose.model('ProyFacilitador');
var User = mongoose.model('User');
var Comentario = mongoose.model('Comentario');
var Documento = mongoose.model('Documento');
var Hito = mongoose.model('Hito');
var ObjectId = mongoose.Types.ObjectId;

module.exports.getColumna = function(req, res){
  Columna.findOne({_id:new ObjectId(req.query.colId)})
  .populate('tareas')
  .exec(function(err, columnaRecuperada){
    if(err){
      res.status(500).json({error: "Al recuperar la columna"});
    }
    else{
      console.log("Encontro la columna "+columnaRecuperada.nombre);
      res.status(200).json(columnaRecuperada);
    }
  });
};

module.exports.getLastTarea = function(req, res){
  console.log("En getLastTarea");
  Proyecto.findOne({_id: new ObjectId(req.query.proyId)})
  .exec(function(err, proyRecuperado){
    if(err){
      res.status(500).json({error:"Al recuperar el proyecto."});
    }
    else{
      Columna.find({"_id":{"$in":proyRecuperado.columnas}})
      .populate('tareas')
      .exec(function(err2,columnasRecuperadas){
        if(err2){
          console.log("ERROR: Al recuperar las columnas asociadas");
          res.status(500).json({error:"Al recuperar las columnas asociadas"});
        }
        else{
          var tareas = [];
          for (var i = 0; i < columnasRecuperadas.length; i++) {
            tareas = tareas.concat(columnasRecuperadas[i].tareas);
          }
          tareas.sort(function(a, b){
            var fechaa = new Date(a.fechaCreacion);
            var fechab = new Date(b.fechaCreacion);
            return fechaa-fechab;
          });
          console.log("Ultima tarea:");
          if(tareas[0]){
            res.status(200).json(tareas[0].fechaCreacion);
          }
          else{
            res.status(200).json(proyRecuperado.fecha);
          }
          console.log(tareas[0]);
        }

      });
    }
  });

}

module.exports.getTarea = function(req, res){
  console.log("Buscando el proyecto: "+req.query.proyId);
  Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
  .populate("columnas")
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto en el que pertenece la tarea", err:err});
    }
    else{
      proyectoEncontrado.columnas.forEach(function(col, index){
        if(col.tareas.indexOf(req.query.taskId)>=0){

          Tarea.findOne({_id: new ObjectId(req.query.taskId)})
          .lean()
          .populate('creador documentos responsables')
          .exec(function(err2,tareaRecuperada){
            if(err2){
              res.status(500).json({error:"Al recuperar la tarea",err:err2});
            }
            else{
              //el objeto que entrega mongoose no es editable, por eso lo transformo a uno que si es editable :)
              //otra opcion es llamar al metodo .lean() antes de .exec() para que devuelva un objeto editable
              //var tareaEditable = tareaRecuperada.toObject();
              Comentario.find({_id:{$in: tareaRecuperada.comentarios}})
              .populate('autor')
              .exec(function(err3, comentarios){
                if(err3){
                  res.status(500).json({error:"Al recuperar los comentarios asociados",err:err3});
                }
                else{
                  console.log("Encontro la tarea "+tareaRecuperada.nombre);
                  console.log("En la columna:"+col.nombre);
                  tareaRecuperada.comentarios = comentarios;
                  tareaRecuperada.columna = col.nombre;
                  res.status(200).json(tareaRecuperada);
                }
              });
            }
          });
        }
      })
    }
  });
};

module.exports.getColumnas = function(req, res){
  Proyecto.findOne({_id: req.query.proyId})
  .populate("columnas")
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al recuperar el proyecto", err:err});
    }
    else{
      res.status(200).json(proyectoEncontrado.columnas);
    }
  });
};

module.exports.getColumnasTareas = function(req, res) {
   if(!req.query.proyId){//debo mover esa verificacion al lado del cliente!!!!
     res.status(500).json({
       "message" : "ERROR: No hay un proyecto seleccionado",
       missing : true
     });
   }
   else{
      console.log("En getColumnas")
      Proyecto.findOne({_id:new ObjectId(req.query.proyId)})
      .exec(function(err, proyRecuperado){
        if(err || proyRecuperado==null){
          console.log("ERROR: Al recuperar el proyecto que tiene las columnas");
          res.status(500).json({error:"Al recuperar el proyecto que tiene las columnas.", err:err});
        }
        else{
          Columna.find({"_id":{"$in":proyRecuperado.columnas}})
          .exec(function(err2,columnas){
            if(err2){
              console.log("ERROR: Al recuperar las columnas asociadas");
              res.status(500).json({error:"Al recuperar las columnas asociadas"});
            }
            else{
              var cant = columnas.length;
              var huboError = false;
              var error = null;
              columnas.forEach(function(col, index){
                Tarea.find({_id:{$in:col.tareas}})
                .populate("responsables")
                .exec(function(errTarea, tareasRecuperadas){
                  if(errTarea){
                    huboError = true;
                    error = errTarea;
                  }
                  else{
                    columnas[index].tareas = tareasRecuperadas;
                  }
                  if(!--cant){
                    if(huboError){
                      res.status(500).json({error:"Al recuperar las tareas de cada columna", err:error});
                    }
                    else{
                      console.log("columnas recuperadas");
                      res.status(200).json(columnas);
                    }
                  }
                });

              });
            }

          });

        }
      });
   }
};

module.exports.insertComentario = function(req, res){
  Tarea.findOne({_id: new ObjectId(req.body.taskId)})
  .exec(function(err, tareaRecuperada){
    if(err){
      console.log("Al buscar la tarea donde ingresar el comentario");
      res.status(500).json({error:"Al buscar la tarea donde ingresar el comentario"});
    }
    else {
      var comentario = new Comentario();
      comentario.fecha = new Date();
      comentario.autor = req.body.userId;
      comentario.texto = req.body.comentario;
      console.log(comentario);
      comentario.save(function(err2, comentarioGuardado){
        if(err2){
          console.log("Al guardar el comentario");
          console.log(err2);
          res.status(500).json({error:"Al guardar el comentario"});
        }
        else {
          tareaRecuperada.comentarios.push(comentarioGuardado._id);
          tareaRecuperada.save(function(err3, tareaActualizada){
            if(err3){
              res.status(500).json({error:"Al actualizar la tarea"});
            }
            else{
                res.status(200).json(comentarioGuardado);
            }
          });
        }
      });
      /*tareaRecuperada.comentarios.push(comentario);
      tareaRecuperada.save(function(err2, tareaActualizada){
        if(err2){
          res.status(500).json({error:"Al actualizar la tarea"});
        }
        else{
            res.status(200).json(comentario);
        }
      });*/
    }
  });
}

module.exports.insertColumna = function(req, res) {
  Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
  .exec(function(err, proyRecuperado){
    if(err || proyRecuperado==null){
      res.status(500).json({error:"Al recuperar proyecto asociado.", err:err});
    }
    else{
      var columna = new Columna();
      columna.nombre = req.body.nombre;
      columna.orden = proyRecuperado.columnas.length;
      //columa.wip = 3;
      columna.tareas = [];
      columna.save(function(err, nuevaColumna){
        if(err){
          res.status(500).json({error:"Al guardar la nueva columna.", err:err});
        }
        else{
          proyRecuperado.columnas.push(columna._id);
          proyRecuperado.save(function(err3, proyActualizado){
            if(err3){
              res.status(500).json({error:"Al actualizar el proyecto asociado.", err:err3});
            }
            else{
              res.status(200).json(columna);
            }
          });
        }
      });
    }
  });
}


module.exports.insertTarea = function(req, res) {
  var creaTarea = function(datos){
    //console.log(datos);
    Columna.findOne({_id : new ObjectId(datos.colid)})
    .exec(function(err, columna){
      if(err){
        console.log("Al buscar la columna a la que debe pertenecer la tarea.");
        res.status(500).json({error:"Al buscar la columna a la que debe pertenecer la tarea.",err:err});
      }
      else{
        var tarea = new Tarea();
        tarea.nombre = datos.nombre;
        tarea.descripcion = datos.descripcion;
        //tarea.columna = req.body.columnId;
        tarea.etiqueta = null;
        tarea.fechaCreacion = new Date();
        var ini = datos.fechaini.split("/");
        tarea.fechaini = new Date(ini[2],ini[1],ini[0]);
        var fin = datos.fechafin.split("/");
        tarea.fechafin = new Date(fin[2],fin[1],fin[0]);
        tarea.orden = columna.tareas.length;//con esto elimino la necesidad de enviar taskLength al momento de crear una tarea :D
        //tarea.orden = datos.orden;
        tarea.responsables = [];
        datos.responsables = JSON.parse(datos.responsables);
        console.log("RESPONSABLES:"+datos.responsables.length);
        for (var i = 0; i < datos.responsables.length; i++) {
          console.log("Insertando:"+datos.responsables[i].name);
          tarea.responsables.push(datos.responsables[i]._id);
        }
        tarea.creador = datos.creador;//comprobar si esto lo guarda bien!
        tarea.comentarios = [];
        tarea.documentos = [];

        var guardaTarea = function(tareaLocal){
          tareaLocal.save(function(err2, nuevaTarea){
            if(err2){
              console.log("ERROR: Al insertar una nueva tarea.");
              res.status(500).json({error:"Al insertar una nueva tarea.",err:err2});
            }
            else{
              //insertar el id de la tarea en el arreglo de id de tareas en la columna
              columna.tareas.push(nuevaTarea._id);
              columna.save(function(err3, colActualizada){
                if(err3){
                  console.log("Al actualizar la columna.");
                  res.status(500).json({error:"Al actualizar la columna.",err:err3});
                }
                else{
                  if(documento!=null){
                    console.log("Si habia documento");
                    console.log(documento);
                    var base = __dirname.split("/");
                    base.pop();//quito controllers
                    base.pop();//quito app_api
                    var ruta = base.join("/")+"/public/documents/";
                    var ext = path.extname(documento.name);
                    documento.mv(ruta+nuevaTarea.documentos[0]+ext,function(err4){
                      if(err4){
                        res.status(500).json({error:"Al guardar el documento", err:err4});
                      }
                      else{
                        res.status(200).json(nuevaTarea);
                      }
                    });
                  }
                  else{
                      res.status(200).json(nuevaTarea);
                  }
                }
              });
            }
          });
        };


        var documento = null;
        if(req.files){
          documento = req.files.documentos[0];//en el lado del cliente defini que solo pueda subir un archivo
          var doc = new Documento();
          doc.nombre = documento.name;
          doc.fecha = new Date();
          doc.proyecto = datos.proyId;
          doc.save(function(errDoc, docGuardado){
            if(errDoc){//si hubo error hasta ahora no hago nada, pero deberia!!!
              console.error(errDoc);
            }
            else{
              tarea.documentos.push(docGuardado._id);
              guardaTarea(tarea);
            }
          });
        }
        else{
          guardaTarea(tarea);
        }
      }
    });
  };

  console.log("en insetTarea");
  console.log(req.body);
  var colid = req.body.columnId;
  if(colid=='0'){
    Proyecto.findOne({_id:new ObjectId(req.body.proyId)})
    .exec(function(err, proyectoEncontrado){
      if(err){
        res.status(500).json({error:"Al buscar el proyecto al que pertenece"});
      }
      else{
        //console.log(proyectoEncontrado);
        creaTarea({proyId:req.body.proyId, colid:proyectoEncontrado.colDefecto, fechaini:req.body.fechaini, fechafin: req.body.fechafin,nombre:req.body.nombre,descripcion:req.body.descripcion,orden:req.body.orden,responsables:req.body.responsables,creador:req.body.creador});
      }
    });
  }
  else{
    creaTarea({proyId:req.body.proyId,colid:colid,fechaini: req.body.fechaini,fechafin: req.body.fechafin,nombre:req.body.nombre,descripcion:req.body.descripcion,orden:req.body.orden,responsables:req.body.responsables,creador:req.body.creador});
  }
};

module.exports.setColumna = function(req, res) {
  var colid = req.body.colid;
  var nombre = req.body.nombre;

  Columna.findOne({_id: colid})
  .exec(function(err, columna){
    if(err){
      res.status(500).json({error:"Al buscar la columna.",err:err});
    }
    else{
      columna.nombre = nombre;
      columna.save(function(err2, columnaActualizada){
        if(err2){
          res.status(500).json({error:"Al actualizar la columna", err:err2});
        }
        else{
          res.status(200).send("OK");
        }
      });
    }
  });
};

module.exports.setTarea = function(req, res) {
  var tarea = req.body.tarea;

  Tarea.findOne({_id:new ObjectId(tarea._id)})
  .exec(function(err, tareaEncontrada){
    if(err){
      res.status(500).json({error:"Al buscar la tarea a editar", err:err});
    }
    else{
      /*
      var tareaSchema = new mongoose.Schema({
        nombre : { type: String, required: true },
        descripcion : {type: String, required: true },
        fechaCreacion:{type: Date },
        fechaini: {type: Date },
        fechafin: {type: Date },
        orden: {type: Number},
        responsables: [{type: Schema.Types.ObjectId, ref: 'User'}],
        etiqueta : {type: Schema.Types.ObjectId, ref: 'Etiqueta'},
        creador: {type: Schema.Types.ObjectId, ref: 'User'},
        actualizador : {type: Schema.Types.ObjectId, ref: 'User'},
        documentos : [{type: Schema.Types.ObjectId, ref: 'Documento'}],
        //columna : {type: Schema.Types.ObjectId, ref: 'Columna'},
        comentarios:[{type: Schema.Types.ObjectId, ref: 'Comentario'}]
        //comentarios: [comentariosSchema]
      },{
          timestamps: true
      });
      mongoose.model('Tarea', tareaSchema);
      */
      tareaEncontrada.nombre = tarea.nombre;
      tareaEncontrada.descripcion = tarea.descripcion;
      tareaEncontrada.fechaini = tarea.fechaini;
      tareaEncontrada.fechafin = tarea.fechafin;
      tareaEncontrada.responsables = tarea.responsables;
      tareaEncontrada.actualizador = tarea.actualizador;
    }
  });
};

module.exports.setTareas = function(req, res){
  var origen = req.body.origen;
  var destino = req.body.destino;
  var tareas = origen.tareas.concat(destino.tareas);

  Columna.findById(origen._id,function(err, col){
    if(err){
      res.status(500).json({error:"Al actualizar alguna tarea"});
    }
    else{
      Columna.findById(destino._id,function(err2, col2){
        if(err){
          res.status(500).json({error:"Al actualizar alguna tarea"});
        }
        else{
          col.tareas = [];
          for (var i = 0; i < origen.tareas.length; i++) {
            col.tareas.push(origen.tareas[i]._id);
          }
          col2.tareas = [];
          for (var i = 0; i < destino.tareas.length; i++) {
            col2.tareas.push(destino.tareas[i]._id);
          }
          col.save();//aqui estoy corriendo un riesgo ya que no compruebo que realmente se haga...
          col2.save();
        }
      });
    }

  });
  var cont = tareas.length;
  var huboError = false;
  //con este for actualizo el orden de cada tarea
  for (var i = 0; i < tareas.length; i++) {
    Tarea.update({_id:tareas[i]._id},{orden:tareas[i].orden}, function(err, tarea){
      if(err){
        huboError = true;
      }
      if(!--cont){
        if(huboError){
          res.status(500).json({error:"Al actualizar alguna tarea"});
        }
        else{
          res.status(200).json({mensaje:"Todo ok"});
        }
      }
    });
  }
};


//funcion que borra la referencia de la tarea en la columna

module.exports.delReferenciaTarea = function(req, res){
  var taskId = req.query.taskId;
  var proyId = req.query.proyId;
  Proyecto.findOne({_id:new ObjectId(proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto donde se encuentra la tarea.",err:err});
    }
    else{
      Columna.find({_id:{$in:proyectoEncontrado.columnas}})
      .exec(function(err2,columnasEncontradas){
        if(err2){
          res.status(500).json({error:"Al recuperar las columnas del proyecto asociado", err:err2});
        }
        else{
          var quitar = -1;
          var columnaActualizar;
          for (var i = 0; i < columnasEncontradas.length; i++) {
            for (var j = 0; j < columnasEncontradas[i].tareas.length; j++) {
              if(columnasEncontradas[i].tareas[j] == taskId){
                console.log("La tarea esta en la posicion "+j);
                quitar = j;
                break;
              }
            }
            if(quitar>=0){
              columnaActualizar = columnasEncontradas[i];
              columnaActualizar.tareas.splice(quitar,1);
              break;
            }
          }

          columnaActualizar.save(function(err3, columnaActualizada){
            if(err3){
              res.status(500).json({error:"Al actualizar el proyecto que contiene la tarea eliminada.",err:err3});
            }
            else{
              res.status(200).send("OK");
            }
          });
        }
      });
    }
  });
};


//funcion que borra la refenrencia de la tarea en una columna y el dato en la coleccion de tareas
module.exports.delTarea = function(req, res){
  var taskId = req.query.taskId;
  var proyId = req.query.proyId;
  Proyecto.findOne({_id:new ObjectId(proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto donde se encuentra la tarea.",err:err});
    }
    else{
      Columna.find({_id:{$in:proyectoEncontrado.columnas}})
      .exec(function(err2,columnasEncontradas){
        if(err2){
          res.status(500).json({error:"Al recuperar las columnas del proyecto asociado", err:err2});
        }
        else{
          var quitar = -1;
          var columnaActualizar;
          for (var i = 0; i < columnasEncontradas.length; i++) {
            for (var j = 0; j < columnasEncontradas[i].tareas.length; j++) {
              if(columnasEncontradas[i].tareas[j] == taskId){
                console.log("La tarea esta en la posicion "+j);
                quitar = j;
                break;
              }
            }
            if(quitar>=0){
              columnaActualizar = columnasEncontradas[i];
              columnaActualizar.tareas.splice(quitar,1);
              break;
            }
          }

          Tarea.findByIdAndRemove(taskId, function(err2,tareaEliminada){
            if(err2){
              res.status(500).json({error:"Al remover la tarea"});
            }
            else{
              columnaActualizar.save(function(err3, columnaActualizada){
                if(err3){
                  res.status(500).json({error:"Al actualizar el proyecto que contiene la tarea eliminada.",err:err3});
                }
                else{
                  res.status(200).send("OK");
                }
              });
            }
          });
        }
      });
    }
  });
};


//borra la tarea, su referencia en el kanban y en la hoja de ruta.
module.exports.delTareaHito = function(req, res){
  var taskId = req.query.taskId;
  var proyId = req.query.proyId;

  var borrarTarea = function(){
    Tarea.findByIdAndRemove(taskId, function(err2,tareaEliminada){
      if(err2){
        res.status(500).json({error:"Al remover la tarea"});
      }
      else{
        res.status(200).send("OK");
      }
    });
  };


  Proyecto.findOne({_id:new ObjectId(proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto donde se encuentra la tarea.",err:err});
    }
    else{
      var cant = 2;
      var huboError = false;
      var errores = [];
      Columna.find({_id:{$in:proyectoEncontrado.columnas}})
      .exec(function(err2,columnasEncontradas){
        if(err2){
          //res.status(500).json({error:"Al recuperar las columnas del proyecto asociado", err:err2});
          huboError = true;
          errores.push({error:"Al recuperar las columnas del proyecto asociado",err:err2});
        }
        else{
          var quitar = -1;
          var columnaActualizar;
          for (var i = 0; i < columnasEncontradas.length; i++) {
            for (var j = 0; j < columnasEncontradas[i].tareas.length; j++) {
              if(columnasEncontradas[i].tareas[j] == taskId){
                console.log("La tarea esta en la posicion "+j);
                quitar = j;
                break;
              }
            }
            if(quitar>=0){
              columnaActualizar = columnasEncontradas[quitar];
              columnaActualizar.tareas.splice(quitar,1);
              break;
            }
          }
          columnaActualizar.save(function(err3, columnaActualizada){
            if(err3){
              res.status(500).json({error:"Al actualizar la columna que contenia la tarea eliminar.",err:err3});
            }
            else{
              if(!--cant){
                if(huboError){
                  res.status(500).json(errores);
                }
                else{
                  borrarTarea();
                }
              }
            }
          });
        }
      });

      Hito.find({_id: {$in: proyectoEncontrado.hitos}})
      .exec(function(err2, hitosEncontrados){
        if(err2){
          huboError = true;
          errores.push({error:"Al recuperar los hitos asociados al proyecto asociado.",err:err2});
        }
        else{
          var hitoSeleccionado = null;
          var pos = -1;
          for (var i = 0; i < hitosEncontrados.length; i++) {
            pos = hitosEncontrados[i].tareas.indexOf(taskId);
            if(pos >= 0){
              hitoSeleccionado = hitosEncontrados[i];
              break;
            }
          }

          if(hitoSeleccionado!=null){
            hitoSeleccionado.tareas.splice(pos, 1);
            hitoSeleccionado.save(function(err3, hitosActualizados){
              if(err3){
                res.status(500).json({error:"Al actualizar el hito que contenia la tarea a eliminar.",err:err3});
              }
              else{
                if(!--cant){
                  if(huboError){
                    res.status(500).json(errores);
                  }
                  else{
                    borrarTarea();
                  }
                }
              }
            });
          }
          else{
            if(!--cant){
              borrarTarea();
            }
          }
        }
      });
    }
  });

}

module.exports.delColumna = function(req, res){
  //var proyId = req.body.proyId || req.query.proyId;
  var proyId = req.query.proyId;
  //var colId = req.body.colId || req.query.colId;
  var colId = req.query.colId;
  Proyecto.findOne({_id: new ObjectId(proyId)})
  .exec(function(err, proyectoEncontrado){
    if(err){
      res.status(500).json({error:"Al buscar el proyecto asociado.", err:err});
    }
    else{
      Columna.findByIdAndRemove(colId, function(err2, columnaEliminada){
        if(err2){
          res.status(500).json({error:"Al remover una columna.",err:err2});
        }
        else{
          console.log(columnaEliminada);
          Columna.findOne({_id:proyectoEncontrado.colDefecto})
          .exec(function(err3, columnaPorDefecto){
            if(err3){
              res.status(500).json({error:"Al buscar la columna por defecto", err:err3});
            }
            else{
              columnaPorDefecto.tareas = columnaPorDefecto.tareas.concat(columnaEliminada.tareas);
              columnaPorDefecto.save(function(err4, columnaActualizada){
                if(err4){
                  res.status(500).json({error:"Al actualizar las tareas de la columna por defecto", err:err4});
                }
                else{
                  res.status(200).json(columnaActualizada);
                }
              });
            }
          });
          /*proyectoEncontrado.colDefecto.tareas.concat(columnaAEliminar.tareas);
          proyectoEncontrado.colDefecto.save(function(err3, colDefectoActualizada){
            if(err3){
              res.status(500).json({error:"Al mover las tareas a la columna por defecto", err:err3});
            }
            else{
              res.status(200).send("OK");
            }
          });*/
        }
      });
    }
  });
};
